package ortu.pinisi.io.ui.fragment.home;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.orhanobut.hawk.Hawk;
import com.orhanobut.hawk.NoEncryption;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ortu.pinisi.io.data.apihelper.BaseApiService;
import ortu.pinisi.io.data.apihelper.UtilsApi;
import ortu.pinisi.io.R;
import ortu.pinisi.io.model.studentfinalmark.Quize;
import ortu.pinisi.io.model.Constant;
import ortu.pinisi.io.ui.adapter.AbsentAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("SimpleDateFormat")
public class ListMainFragment extends Fragment implements ListMainPresenter.ListMainListener {

    @BindView(R.id.listAbsent)
    RecyclerView listAbsen;

    @BindView(R.id.nothingAbsent)
    TextView nothingAbsen;

    @BindView(R.id.dateNow)
    TextView dateNow;

    @BindView(R.id.refresh)
    SwipeRefreshLayout refreshLayout;

    @BindView(R.id.setDate)
    Button setDate;

    List<Quize> list = new ArrayList<>();
    List<ortu.pinisi.io.model.absent.Result> listAbsent = new ArrayList<>();
    List<ortu.pinisi.io.model.absent.Result> exceptAbsent = new ArrayList<>();
    List<ortu.pinisi.io.model.absent.Result> listAbsentLesson = new ArrayList<>();
    List<String> listDummy = new ArrayList<>();
    String[] MONTH = {"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus",
            "September", "Oktober", "November", "Desember"};
    String url,dateView,date,dateDb;

    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;

    ListMainPresenter listMainPresenter;
    BaseApiService mApiService;

    public ListMainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String string_date = "2017-12-09";

        Date curDate = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatView = new SimpleDateFormat("dd MMMM yyyy");
        date = format.format(curDate);
        dateView = formatView.format(curDate);

        Hawk.init(getContext())
                .setEncryption(new NoEncryption())
                .build();

        url = Hawk.get(Constant.Key.KEY_URL);

        mApiService = UtilsApi.getAPIService(getContext(), url);

        listMainPresenter = new ListMainPresenter(getContext(),mApiService,this);
        listMainPresenter.getAbsentData(date);
        listMainPresenter.getScores(date);
        listMainPresenter.getAbsentLesson(date);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list_main, container, false);

        ButterKnife.bind(this, view);

        listDummy.add("1");
        listDummy.add("2");
        listDummy.add("3");

        refreshLayout.setColorSchemeResources(R.color.red,R.color.blue,R.color.colorAccent);
        refreshLayout.setRefreshing(true);

        // show data in recyclerview
        refreshLayout.setOnRefreshListener(() -> {
            listAbsent.clear();
            exceptAbsent.clear();
            listAbsentLesson.clear();
            list.clear();

            if (dateDb != null){
                dateNow.setText(dateView);
                mAdapter.notifyDataSetChanged();

                listMainPresenter.getAbsentData(dateDb);
                listMainPresenter.getScores(dateDb);
                listMainPresenter.getAbsentLesson(dateDb);
            } else {
                dateNow.setText(dateView);
                mAdapter.notifyDataSetChanged();

                listMainPresenter.getAbsentData(date);
                listMainPresenter.getScores(date);
                listMainPresenter.getAbsentLesson(date);
            }

            reloadAllData();
        });

        reloadAllData();

        dateNow.setText(dateView);

        return view;
    }

    @OnClick(R.id.setDate)
    public void onBtnDateClicked(){
        mAdapter.notifyDataSetChanged();
        listAbsent.clear();
        exceptAbsent.clear();
        listAbsentLesson.clear();
        list.clear();
        final DatePickerDialog.OnDateSetListener mDatesetListener = (view, year, month, dayOfMonth) -> {
            dateNow.setText(dayOfMonth+" "+MONTH[month]+" "+year);
            dateView = dayOfMonth+" "+MONTH[month]+" "+year;
            month = month + 1;
            if (dayOfMonth < 10 && month < 10){
                dateDb = year+"-0"+month+"-0"+dayOfMonth;
            }else if (dayOfMonth < 10){
                dateDb = year+"-"+month+"-0"+dayOfMonth;
            }else if (month < 10){
                dateDb = year+"-0"+month+"-"+dayOfMonth;
            }else{
                dateDb = year+"-"+month+"-"+dayOfMonth;
            }

            Log.d("date_db",dateDb);
            listMainPresenter.getAbsentData(dateDb);
            listMainPresenter.getScores(dateDb);
            listMainPresenter.getAbsentLesson(dateDb);
            refreshLayout.setRefreshing(true);
            reloadAllData();
        };
        Calendar calender = Calendar.getInstance();

        Dialog mDialog = new DatePickerDialog(getContext(), mDatesetListener, calender.get(Calendar.YEAR),
                calender.get(Calendar.MONTH), calender
                .get(Calendar.DAY_OF_MONTH));

        mDialog.show();
    }

    private void reloadAllData() {
        // get new modified random data
        mLayoutManager = new LinearLayoutManager(getContext());
        listAbsen.setLayoutManager(mLayoutManager);

        new Handler().postDelayed(() -> {
            mAdapter = new AbsentAdapter(getContext(),list,listAbsentLesson,listAbsent,exceptAbsent,dateView);
            listAbsen.setAdapter(mAdapter);
            refreshLayout.setRefreshing(false);
        },2000);

        if (listAbsentLesson == null){
            nothingAbsen.setVisibility(View.VISIBLE);
        }

        if (listAbsentLesson.size() <= 0){
            nothingAbsen.setVisibility(View.VISIBLE);
        }

        if (listAbsentLesson.size() > 0){
            nothingAbsen.setVisibility(View.GONE);
            nothingAbsen.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void examScoresRespone(List<Quize> results) {
        list = results;

        reloadAllData();
    }

    @Override
    public void absentLessonRespone(List<ortu.pinisi.io.model.absent.Result> results) {
        exceptAbsent = new ArrayList<>();
        for (ortu.pinisi.io.model.absent.Result result : results){
            if (result.getAbsen().equals("Hadir")){
                listAbsent.add(result);
                Log.d("rowAbsent",""+result.getAbsen());
            }else{
                if (results.size() > 0){
                    exceptAbsent.add(result);
                    Log.d("rowAbsent",""+result.getAbsen());
                }
            }
        }
        listAbsentLesson = results;

        reloadAllData();
    }
}