package ortu.pinisi.io.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ortu.pinisi.io.R;
import ortu.pinisi.io.model.absent.Absent;
import ortu.pinisi.io.model.absent.Result;

/**
 * Created by mfahm on 2/19/2018.
 */

public class AbsentLessonAdapter extends RecyclerView.Adapter<AbsentLessonAdapter.ViewHolder> {

    Context context;
    List<Result> list;

    public AbsentLessonAdapter(Context context, List<Result> list) {
        this.list = new ArrayList<>();
        this.context = context;
        this.list = list;
    }

    @Override
    public AbsentLessonAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_absen, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AbsentLessonAdapter.ViewHolder holder, int position) {
        holder.lessonName.setText(list.get(position).getLessonName());
        holder.absentValue.setText(list.get(position).getAbsen());
        String absen = list.get(position).getAbsen();
        int color;
        if (absen.equals("Hadir")) {
            color = Color.parseColor("#00bbd3");
        } else if (absen.equals("Sakit")) {
            color = Color.parseColor("#ff7700");
        } else if (absen.equals("Izin")) {
            color = Color.parseColor("#ababab");
        }else{
            color = Color.RED;
        }

        holder.absentValue.setTextColor(color);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView lessonName, absentValue;

        public ViewHolder(View itemView) {
            super(itemView);
            lessonName = (TextView) itemView.findViewById(R.id.lessonName);
            absentValue = (TextView) itemView.findViewById(R.id.absentValue);
        }
    }
}
