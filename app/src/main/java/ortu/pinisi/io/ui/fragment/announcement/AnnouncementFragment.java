package ortu.pinisi.io.ui.fragment.announcement;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.orhanobut.hawk.Hawk;

import butterknife.BindView;
import butterknife.ButterKnife;
import ortu.pinisi.io.data.apihelper.BaseApiService;
import ortu.pinisi.io.data.apihelper.UtilsApi;
import ortu.pinisi.io.R;
import ortu.pinisi.io.ui.adapter.AnnouncementAdapter;
import ortu.pinisi.io.model.studentannouncements.Result;
import ortu.pinisi.io.model.Constant;
import ortu.pinisi.io.utils.util;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class AnnouncementFragment extends Fragment implements AnnouncementPresenter.AnnouncementPresenterListener {

    @BindView(R.id.nothingAnnouncement)
    TextView nothing;

    @BindView(R.id.listanak)
    RecyclerView rvAnnouncement;

    @BindView(R.id.swipeToRefresh)
    SwipeRefreshLayout mSwipeRefreshLayout;

    Runnable refresh;
    Handler handler = new Handler();
    Context mContext;
    BaseApiService mApiService;
    List<Result> listAnnouncement;

    String url;

    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private AnnouncementPresenter announcementPresenter;

    public AnnouncementFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        ButterKnife.bind(this, view);

        url = Hawk.get(Constant.Key.KEY_URL);

        mContext = getContext();
        mApiService = UtilsApi.getAPIService(getContext(),url); // meng-init yang ada di package apihelper

        announcementPresenter = new AnnouncementPresenter(getContext(),this,mApiService);
        announcementPresenter.getAnnouncementData();

        mLayoutManager = new LinearLayoutManager(getContext());
        rvAnnouncement.setLayoutManager(mLayoutManager);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.red,R.color.blue);
        mSwipeRefreshLayout.setRefreshing(true);

        // show data in recyclerview

        mSwipeRefreshLayout.setOnRefreshListener(() -> {

            mAdapter.notifyDataSetChanged();
            announcementPresenter.getAnnouncementData();

        });
        // Inflate the layout for this fragment
        util.setMixPanel(mContext, "PengumumanActivity");

        return view;
    }

    @Override
    public void getAnnouncement(List<Result> news) {
        if (news != null){
            listAnnouncement = new ArrayList<Result>();
            listAnnouncement = news;
            if (listAnnouncement.size() == 0){
                nothing.setVisibility(View.VISIBLE);
            }else{
                reloadAllData();
            }
        }else{
            mSwipeRefreshLayout.setRefreshing(false);
            nothing.setVisibility(View.VISIBLE);
        }
    }

    private void reloadAllData(){
        // get new modified random data
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run(){

                mAdapter = new AnnouncementAdapter(getContext(),listAnnouncement);
                rvAnnouncement.setAdapter(mAdapter);

                mSwipeRefreshLayout.setRefreshing(false);
            }
        },1500);
    }
}
