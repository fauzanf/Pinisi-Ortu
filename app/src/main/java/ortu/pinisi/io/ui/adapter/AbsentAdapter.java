package ortu.pinisi.io.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ortu.pinisi.io.R;
import ortu.pinisi.io.model.studentabsen.Result;
import ortu.pinisi.io.model.studentfinalmark.Quize;

/**
 * Created by mfahm on 1/16/2018.
 */

public class AbsentAdapter extends RecyclerView.Adapter<AbsentAdapter.ViewHolder> {

    private final Context context;
    private List<Quize> listPengetahuan;
    private List<ortu.pinisi.io.model.absent.Result> listAbsent;
    private List<ortu.pinisi.io.model.absent.Result> firsRow;
    private List<ortu.pinisi.io.model.absent.Result> exceptAbsent;
    private String date;

    public AbsentAdapter(Context context, List<Quize> listPengetahuan,
                         List<ortu.pinisi.io.model.absent.Result> listAbsent,
                         List<ortu.pinisi.io.model.absent.Result> firsRow,
                         List<ortu.pinisi.io.model.absent.Result> exceptAbsent, String date) {
        this.listPengetahuan = new ArrayList<>();
        this.listAbsent = new ArrayList<>();
        this.firsRow = new ArrayList<>();
        this.exceptAbsent = new ArrayList<>();
        this.context = context;
        this.listPengetahuan = listPengetahuan;
        this.listAbsent = listAbsent;
        this.firsRow = firsRow;
        this.exceptAbsent = exceptAbsent;
        this.date = date;
    }

    @Override
    public AbsentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_home, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AbsentAdapter.ViewHolder holder, int position) {
        String absent = null;

        if (firsRow.size() == 0 && exceptAbsent.size() > 0) {
            holder.employeesName.setText(exceptAbsent.get(position).getNama());
            absent = exceptAbsent.get(position).getAbsen();
        } else if (firsRow.size() > 0 && exceptAbsent.size() > 0) {
            holder.employeesName.setText(firsRow.get(position).getNama());
            absent = firsRow.get(position).getAbsen();
        } else {
            holder.employeesName.setText(firsRow.get(position).getNama());
            absent = firsRow.get(position).getAbsen();
        }
        String charAbsent;
        int color;
        if (absent.equals("Hadir")) {
            color = Color.parseColor("#00bbd3");
            charAbsent = "H";
        } else if (absent.equals("Sakit")) {
            color = Color.parseColor("#ff7700");
            charAbsent = "S";
        } else if (absent.equals("Izin")) {
            color = Color.parseColor("#ababab");
            charAbsent = "I";
        } else {
            color = Color.RED;
            charAbsent = "A";
        }

        holder.employeesAbsent.setTextColor(color);
        holder.employeesAbsent.setText(charAbsent);
    }

    @Override
    public int getItemCount() {
        if (firsRow.size() > 0) {
            return 1;
        } else if (exceptAbsent.size() > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView nothing, nothingAbsent, nothingSikap, employeesName, employeesAbsent, textName, textNameAbsent;
        ImageView exitImg, exitImgAbsent;
        RecyclerView listNilaiPengetahuan, listNilaiSikap, listAbsentLesson;
        LinearLayout layoutAbsent, layoutNilai;
        private RecyclerView.LayoutManager mLayoutManager, mLayoutManagerSikap, mLayoutManagerAbsent;
        private RecyclerView.Adapter mAdapter, mAdapterAbsent;

        public ViewHolder(View itemView) {
            super(itemView);

            employeesName = (TextView) itemView.findViewById(R.id.studentName);
            employeesAbsent = (TextView) itemView.findViewById(R.id.ketAbsent);
            layoutAbsent = (LinearLayout) itemView.findViewById(R.id.layout_absent);
            layoutNilai = (LinearLayout) itemView.findViewById(R.id.layout_nilai);

            layoutNilai.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    LayoutInflater layoutInflaterAndroid = LayoutInflater.from(context);
                    final View mView = layoutInflaterAndroid.inflate(R.layout.detail_absen, null);
                    AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(context);
                    alertDialogBuilderUserInput.setView(mView);

                    exitImg = (ImageView) mView.findViewById(R.id.imgClose);
                    listNilaiPengetahuan = (RecyclerView) mView.findViewById(R.id.listNilaiPengetahuan);
                    listNilaiSikap = (RecyclerView) mView.findViewById(R.id.listNilaiSikap);
                    nothing = (TextView) mView.findViewById(R.id.nothingKnow);
                    nothingSikap = (TextView) mView.findViewById(R.id.nothingSikap);
                    textName = (TextView) mView.findViewById(R.id.textName);


                    final android.support.v7.app.AlertDialog dialog = alertDialogBuilderUserInput.create();

                    mLayoutManager = new LinearLayoutManager(context);
                    mLayoutManagerSikap = new LinearLayoutManager(context);
                    listNilaiPengetahuan.setLayoutManager(mLayoutManager);
                    listNilaiSikap.setLayoutManager(mLayoutManagerSikap);

                    textName.setText(listAbsent.get(position).getNama() + " . " + date);

                    if (listAbsent.size() <= 0) {
                        nothing.setVisibility(View.VISIBLE);
                        nothingSikap.setVisibility(View.VISIBLE);
                    }

                    if (listPengetahuan.size() <= 0) {
                        nothing.setVisibility(View.VISIBLE);
                    }

                    mAdapter = new NilaiPengetahuanAdapter(context, listPengetahuan);
                    listNilaiPengetahuan.setAdapter(mAdapter);

                    /*mAdapterSikap = new NilaiSikapAdapter(context, listPengetahuan);
                    listNilaiSikap.setAdapter(mAdapterSikap);*/

                    exitImg.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    dialog.show();
                }
            });

            layoutAbsent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    LayoutInflater layoutInflaterAndroid = LayoutInflater.from(context);
                    final View mView = layoutInflaterAndroid.inflate(R.layout.layout_absent, null);
                    AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(context);
                    alertDialogBuilderUserInput.setView(mView);

                    exitImgAbsent = (ImageView) mView.findViewById(R.id.imgClose);
                    listAbsentLesson = (RecyclerView) mView.findViewById(R.id.listAbsentLesson);
                    nothingAbsent = (TextView) mView.findViewById(R.id.nothingKnowAbsent);
                    textNameAbsent = (TextView) mView.findViewById(R.id.textName);

                    mLayoutManagerAbsent = new LinearLayoutManager(context);
                    listAbsentLesson.setLayoutManager(mLayoutManagerAbsent);

                    final android.support.v7.app.AlertDialog dialog = alertDialogBuilderUserInput.create();

                    textNameAbsent.setText(listAbsent.get(position).getNama() + " . " + date);

                    if (listAbsent.size() <= 0) {
                        nothingAbsent.setVisibility(View.VISIBLE);
                    }

                    mAdapterAbsent = new AbsentLessonAdapter(context, listAbsent);
                    listAbsentLesson.setAdapter(mAdapterAbsent);

                    /*mAdapterSikap = new NilaiSikapAdapter(context, listPengetahuan);
                    listNilaiSikap.setAdapter(mAdapterSikap);*/

                    exitImgAbsent.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    dialog.show();
                }
            });
        }
    }
}
