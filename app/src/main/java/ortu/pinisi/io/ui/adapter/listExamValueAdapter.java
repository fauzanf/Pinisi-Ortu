package ortu.pinisi.io.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ortu.pinisi.io.R;
import ortu.pinisi.io.model.studentfinalmark.Quize;

/**
 * Created by mfahm on 1/8/2018.
 */

public class listExamValueAdapter extends RecyclerView.Adapter<listExamValueAdapter.ViewHolder> {

    Context context;
    List<Quize> listExam;

    public listExamValueAdapter(Context context, List<Quize> listExam) {
        this.listExam = new ArrayList<Quize>();
        this.context = context;
        this.listExam = listExam;
    }

    @Override
    public listExamValueAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_ujian, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(listExamValueAdapter.ViewHolder holder, int position) {

        String inputPattern = "yyyy-MM-dd hh:mm:ss";
        String outputPattern = "dd MMMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(listExam.get(position).getCreated_at());
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.numberText.setText(""+(position+1));
        holder.tglText.setText(str);
        holder.titleText.setText(listExam.get(position).getTitle());
        holder.valueText.setText(listExam.get(position).getScore());
        int valueExam = Integer.parseInt(listExam.get(position).getScore());
        if (valueExam >= 75){
            holder.valueText.setTextColor(Color.parseColor("#2ecc71"));
        }else{
            holder.valueText.setTextColor(Color.parseColor("#ff7f00"));
        }
    }

    @Override
    public int getItemCount() {
        return listExam.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView numberText,tglText,titleText,valueText;

        public ViewHolder(View itemView) {
            super(itemView);

            numberText = (TextView)itemView.findViewById(R.id.textNumber);
            tglText = (TextView)itemView.findViewById(R.id.textTgl);
            titleText = (TextView)itemView.findViewById(R.id.textTitle);
            valueText = (TextView)itemView.findViewById(R.id.textValue);
        }
    }
}
