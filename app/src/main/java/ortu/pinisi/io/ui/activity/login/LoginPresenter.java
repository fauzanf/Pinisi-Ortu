package ortu.pinisi.io.ui.activity.login;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ortu.pinisi.io.data.apihelper.BaseApiService;
import ortu.pinisi.io.data.apihelper.UtilsApi;
import ortu.pinisi.io.model.login.Login;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mfahm on 1/15/2018.
 */

public class LoginPresenter {

    private final Context context;
    private final LoginPresenterListener mListener;
    private BaseApiService mApiService;
    private final String url,username,password;

    public LoginPresenter(Context context, LoginPresenterListener mListener, BaseApiService mApiService, String url, String username, String password) {
        this.context = context;
        this.mListener = mListener;
        this.mApiService = mApiService;
        this.url = url;
        this.username = username;
        this.password = password;
    }

    public interface LoginPresenterListener{
        void loginRespone(Login login);
    }

    @SuppressLint("CheckResult")
    public void getUser(){
        mApiService = UtilsApi.getAPIService(context,url); // meng-init yang ada di package apihelper
        mApiService.loginRequest(username,password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mListener::loginRespone, throwable -> showToast(throwable.getMessage())
                );
    }

    public void showToast(String msg){
        Toast.makeText(context,msg,Toast.LENGTH_SHORT).show();
    }

}
