package ortu.pinisi.io.ui.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ortu.pinisi.io.R;
import ortu.pinisi.io.model.Constant;
import ortu.pinisi.io.ui.activity.login.LoginActivity;
import ortu.pinisi.io.ui.activity.main.MainButtomNav;

import com.google.android.gms.ads.AdView;
import com.orhanobut.hawk.Hawk;
import com.orhanobut.hawk.NoEncryption;

public class SplashScreenActivity extends AppCompatActivity {

    private final int SPLASH_DISPLAY_LENGTH = 2000;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        Hawk.init(SplashScreenActivity.this)
                .setEncryption(new NoEncryption())
                .build();

//        AdView adView = new AdView(this);
//        adView.setAdSize(AdSize.BANNER);
//        adView.setAdUnitId("ca-app-pub-2941627758407889/7762304057");

        new Handler().postDelayed(() -> {
            /* Create an Intent that will start the Menu-Activity. */
            //Intent mainIntent = new Intent(SplashScreenActivity.this,MainActivity.class);
            //SplashScreenActivity.this.startActivity(mainIntent);
            isLogin();

            SplashScreenActivity.this.finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }, SPLASH_DISPLAY_LENGTH);

    }

    void isLogin(){
        boolean isLogin ;
        if (Hawk.get(Constant.Key.IS_LOGIN) != null){
            isLogin = Hawk.get(Constant.Key.IS_LOGIN);
            if (!isLogin){
                Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }else {
                Intent intent = new Intent(SplashScreenActivity.this, MainButtomNav.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }else {
            Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }
}
