package ortu.pinisi.io.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.orhanobut.hawk.Hawk;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ortu.pinisi.io.R;
import ortu.pinisi.io.model.studentannouncements.Result;
import ortu.pinisi.io.model.Constant;
import ortu.pinisi.io.ui.activity.announcementActivity.AnnouncementActivity;

/**
 * Created by mfahm on 12/8/2017.
 */

public class AnnouncementAdapter extends RecyclerView.Adapter<AnnouncementAdapter.ViewHolder> {

    Context context;
    List<Result> list;

    public AnnouncementAdapter(Context context, List<Result> list){
        this.list = new ArrayList<Result>();
        this.context = context;
        this.list = list;
    }

    @Override
    public AnnouncementAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_announcement, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AnnouncementAdapter.ViewHolder holder, int position) {
        String inputPattern = "yyyy-MM-dd hh:mm:ss";
        String outputPattern = "dd MMMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(list.get(position).getCreatedAt());
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.dateAnnouncement.setText(str);
        holder.titleAnnouncement.setText(list.get(position).getTitle());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView dateAnnouncement,titleAnnouncement;

        public ViewHolder(View itemView) {
            super(itemView);

            dateAnnouncement = (TextView)itemView.findViewById(R.id.dateAnnouncement);
            titleAnnouncement = (TextView)itemView.findViewById(R.id.titleAnnouncement);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();

                    Hawk.put(Constant.Key.KEY_CONTENT,list.get(position).getContent());
                    context.startActivity(new Intent(context, AnnouncementActivity.class));
                    ((Activity)context).overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });

        }
    }
}
