package ortu.pinisi.io.ui.activity.register;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import ortu.pinisi.io.model.User;
import ortu.pinisi.io.ui.activity.main.MainButtomNav;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity {

    Button selesai;
    EditText e_ktp,e_nama,e_alamat,e_username,e_pass;
    ProgressDialog loading;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference mDatabase;
    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(ortu.pinisi.io.R.layout.activity_register);

        mAuth = FirebaseAuth.getInstance();


        mAuthListener = new FirebaseAuth.AuthStateListener() {
        @Override
        public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
            FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User sedang login
                    Log.d("register", "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User sedang logout
                    Log.d("register_in", "onAuthStateChanged:signed_out");
                }
            }
        };

        e_ktp = (EditText)findViewById(ortu.pinisi.io.R.id.ktp);
        e_nama = (EditText)findViewById(ortu.pinisi.io.R.id.nama);
        e_alamat = (EditText)findViewById(ortu.pinisi.io.R.id.alamat);
        e_username = (EditText)findViewById(ortu.pinisi.io.R.id.username);
        e_pass = (EditText)findViewById(ortu.pinisi.io.R.id.pass);

        if(e_ktp.getText().toString().equals("")){
            e_ktp.setError("No.Ktp masih kosong");
        }

        if(e_nama.getText().toString().equals("")){
            e_nama.setError("Nama harus diisi");
        }

        if(e_username.getText().toString().equals("")){
            e_username.setError("contoh : contoh@example.com");
        }

        if(e_pass.getText().toString().equals("")){
            e_pass.setError("Password harus diisi");
        }

        selesai = (Button) findViewById(ortu.pinisi.io.R.id.sub);
        selesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String username = e_username.getText().toString();
                String pass = e_pass.getText().toString();
                String no = e_ktp.getText().toString();
                String name = e_nama.getText().toString();
                String address = e_alamat.getText().toString();

                progress = ProgressDialog.show(RegisterActivity.this,"","Loading...",false,false);
                SignUp(username,pass,no,name,address);

            }
        });
    }

    /*private void daftar(final String url){
        // Ubah setiap View EditText ke tipe Data String
        final String s_username = e_username.getText().toString().trim();
        final String s_password = e_pass.getText().toString().trim();
        final String s_ktp = e_ktp.getText().toString().trim();
        final String s_nama = e_nama.getText().toString().trim();
        final String s_alamat = e_alamat.getText().toString().trim();
        // Pembuatan Class AsyncTask yang berfungsi untuk koneksi ke Database Server

        class Daftar extends AsyncTask<Void,Void,String> {


            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                loading = ProgressDialog.show(RegisterActivity.this,"Login","Loading...",false,false);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                DataRegister(s);

            }

            @Override
            protected String doInBackground(Void... v) {
                HashMap<String,String> params = new HashMap<>();
                // Sesuaikan bagian ini dengan field di tabel Mahasiswa
                params.put("ktp",s_ktp);
                params.put("nama",s_nama);
                params.put("alamat",s_alamat);
                params.put("username",s_username);
                params.put("password",s_password);

                RequestHandler rh = new RequestHandler();
                String ur="";
                if(url.equals("api.pinisi.io")){
                    ur = "https://"+url+"/api/post/?model=login&type=android";
                }else{
                    ur = "http://"+url+"/api/post/?model=login&type=android";
                }
                String res = rh.sendPostRequest(ur, params);
                return res;
            }
        }
        // Jadikan Class TambahData Sabagai Object Baru
        Daftar ae = new Daftar();
        ae.execute();
    }

    private void DataRegister(String json){
        //Toast.makeText(GetDatabase.this,json,Toast.LENGTH_LONG).show();
        //ArrayList<String> id_class = new ArrayList<>();
        int class_id;
        try {
            // Jadikan sebagai JSON object
            JSONObject jsonObject = new JSONObject(json);

            String token = jsonObject.getString("token");
            String error = jsonObject.getString("error");
            String role = jsonObject.getString("role");
            String uid = jsonObject.getString("uid");

            //getDataOption(url,token);

            JSONObject user = jsonObject.getJSONObject("user");
//            JSONObject isi = user.getJSONObject("name");
//            JSONArray result = jsonObject.getJSONArray("user");
//            for(int i = 0; i < jsonObject.length(); i++) {
//                JSONObject c = result.getJSONObject(i);

            String nama = user.getString("name");
            String email = user.getString("email");

            if(error.equals("FALSE")){
                Intent i = new Intent(RegisterActivity.this, MainButtomNav.class);
                Bundle b = new Bundle();
                b.putString("token",token);
                b.putString("nama", nama);
                b.putString("email", email);
                b.putString("role", role);
//                DatabaseAccess access = DatabaseAccess.getInstance(this);
//                access.open();
//
//
//                access.close();
                finish();

//                session = new SessionManager(getApplicationContext());
//                session.createLoginSession(nama,email,token,url,uid);

                //Toast.makeText(this,nama+","+email+","+token+","+url+","+uid,Toast.LENGTH_LONG).show();

                i.putExtras(b);
                startActivity(i);
            }else{
                Toast.makeText(RegisterActivity.this,error, Toast.LENGTH_LONG).show();}

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }*/

    private void SignUp(String email, String password, String noktp, String nama, String alam){

        mDatabase = FirebaseDatabase.getInstance().getReference("User_"+nama);

        User user = new User();
        user.setNoktp(noktp);
        user.setNama(nama);
        user.setAlamat(alam);
        user.setEmail(email);

        mDatabase.setValue(user);

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d("new_register", "createUserWithEmail:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            progress.dismiss();
                            Toast.makeText(getApplicationContext(), "Registrasi Gagal !",
                                    Toast.LENGTH_SHORT).show();

                        }else{
                            progress.dismiss();
                            Toast.makeText(getApplicationContext(), "Registrasi Berhasil",
                                    Toast.LENGTH_SHORT).show();
                            finish();
                        }

                        // ...
                    }
                });
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }
}
