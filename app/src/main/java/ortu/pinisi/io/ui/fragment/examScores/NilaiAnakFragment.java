package ortu.pinisi.io.ui.fragment.examScores;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.orhanobut.hawk.Hawk;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ortu.pinisi.io.data.apihelper.BaseApiService;
import ortu.pinisi.io.data.apihelper.UtilsApi;
import ortu.pinisi.io.R;
import ortu.pinisi.io.ui.adapter.listExamValueAdapter;
import ortu.pinisi.io.model.studentfinalmark.Quize;
import ortu.pinisi.io.model.Constant;
import ortu.pinisi.io.utils.util;

/**
 * A simple {@link Fragment} subclass.
 */
public class NilaiAnakFragment extends Fragment implements examScoresPresenter.ExamScoresPresenterListener {

    @BindView(R.id.listExamValue)
    RecyclerView listExam;

    @BindView(R.id.semester)
    TextView Semester;

    @BindView(R.id.tahun)
    TextView Tahun;

    @BindView(R.id.nothingExam)
    TextView nothingExam;

    @BindView(R.id.progress)
    ProgressBar progress;

    @BindView(R.id.table_main)
    TableLayout stk;

    List<String> nilai = new ArrayList<>();
    List nilai_ket = new ArrayList();
    List<String> listpelajaran = new ArrayList<>();
    List<String> dateExam = new ArrayList<>();
    List<Quize> examData = new ArrayList<>();
    List<String> month = new ArrayList<>();
    Context mContext;
    BaseApiService mApiService;

    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private examScoresPresenter ExamScoresPresenter;

    String url, monthNow;

    public NilaiAnakFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(ortu.pinisi.io.R.layout.fragment_second, container, false);

        ButterKnife.bind(this, view);

        url = Hawk.get(Constant.Key.KEY_URL);

        //inisialisasi view

        mLayoutManager = new LinearLayoutManager(getContext());
        listExam.setLayoutManager(mLayoutManager);

        mContext = getContext();
        // meng-init yang ada di package apihelper
        mApiService = UtilsApi.getAPIService(getContext(), url);
        setLayout();

        ExamScoresPresenter = new examScoresPresenter(getContext(), this,mApiService);
        ExamScoresPresenter.getScores();

        util.setMixPanel(mContext, "NilaiActivity");

        return view;
    }

    public void init() {

        TableRow tbrow0 = new TableRow(getContext());
        TextView tv1 = new TextView(getContext());
        tv1.setText(" Nama Ujian ");
        tv1.setTextAppearance(getContext(), android.R.style.TextAppearance_DeviceDefault_Medium);
        tv1.setTextColor(Color.BLACK);
        tv1.setBackgroundResource(ortu.pinisi.io.R.drawable.cell_shape);
        tbrow0.addView(tv1);

        TextView tv2 = new TextView(getContext());
        tv2.setText(" Nilai ");
        tv2.setTextAppearance(getContext(), android.R.style.TextAppearance_DeviceDefault_Medium);
        tv2.setTextColor(Color.BLACK);
        tv2.setBackgroundResource(ortu.pinisi.io.R.drawable.cell_shape);
        tbrow0.addView(tv2);

        TextView tv3 = new TextView(getContext());
        tv3.setText(" Tanggal Ujian ");
        tv3.setTextAppearance(getContext(), android.R.style.TextAppearance_DeviceDefault_Medium);
        tv3.setTextColor(Color.BLACK);
        tv3.setBackgroundResource(ortu.pinisi.io.R.drawable.cell_shape);
        tbrow0.addView(tv3);

        stk.addView(tbrow0);

        for (int i = 0; i < nilai.size(); i++) {

            TableRow tbrow = new TableRow(getContext());
            TextView t2v = new TextView(getContext());
            t2v.setText(" " + listpelajaran.get(i).toString() + " ");
            t2v.setTextColor(0xFF000000);
//            t2v.setGravity(Gravity.CENTER);
            t2v.setTextAppearance(getContext(), android.R.style.TextAppearance_Medium);
            t2v.setBackgroundResource(ortu.pinisi.io.R.drawable.cell_shape);
            tbrow.addView(t2v);

            TextView t3v = new TextView(getContext());
            t3v.setText("" + nilai.get(i).toString());
            t3v.setTextColor(Color.parseColor("#000000"));
            t3v.setGravity(Gravity.CENTER);
            t3v.setTextAppearance(getContext(), android.R.style.TextAppearance_Medium);
            t3v.setBackgroundResource(ortu.pinisi.io.R.drawable.cell_shape);
            tbrow.addView(t3v);

            TextView t4v = new TextView(getContext());
            t4v.setText("" + dateExam.get(i).toString());
            t4v.setTextColor(Color.parseColor("#000000"));
            t4v.setGravity(Gravity.CENTER);
            t4v.setTextAppearance(getContext(), android.R.style.TextAppearance_Medium);
            t4v.setBackgroundResource(ortu.pinisi.io.R.drawable.cell_shape);
            tbrow.addView(t4v);

            stk.addView(tbrow);


        }

    }

    @Override
    public void examScoresRespone(List<Quize> quize) {
        if (quize != null){
            mAdapter = new listExamValueAdapter(mContext,quize);
            listExam.setAdapter(mAdapter);
            for (Quize quiz : quize){

                String tglApi = "yyyy-MM-dd hh:mm:ss";
                String month = "MM";
                SimpleDateFormat inputTgl = new SimpleDateFormat(tglApi);
                SimpleDateFormat outputTgl = new SimpleDateFormat(month);

                String inputPattern = "yyyy-MM-dd hh:mm:ss";
                String outputPattern = "dd MMMM yyyy";
                SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
                SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

                Date date = null, dateApi = null;
                String str = null, strTgl = null;

                try {
                    dateApi = inputTgl.parse(quiz.getCreated_at());
                    strTgl = outputTgl.format(dateApi);
                    date = inputFormat.parse(quiz.getCreated_at());
                    str = outputFormat.format(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Log.d("month_now", monthNow + "," + strTgl);
                listpelajaran.add(quiz.getTitle());

                nilai.add(quiz.getScore() + "");
                dateExam.add(str);
            }

            init();
            progress.setVisibility(View.GONE);
        }else{
            progress.setVisibility(View.GONE);
            nothingExam.setVisibility(View.VISIBLE);
        }
    }

    public void setLayout() {
        //get Data from pref
        String semester = Hawk.get(Constant.Key.KEY_SEMESTER);
        String tahun = Hawk.get(Constant.Key.KEY_TAHUN_AJARAN);

        //set semester and year
        Semester.setText("" + semester);
        Tahun.setText("" + tahun);
    }
}
