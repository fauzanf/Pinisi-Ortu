package ortu.pinisi.io.ui.fragment.announcement;

import android.content.Context;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ortu.pinisi.io.data.apihelper.BaseApiService;
import ortu.pinisi.io.model.studentannouncements.Result;
import ortu.pinisi.io.model.Constant;

/**
 * Created by mfahm on 1/15/2018.
 */

public class AnnouncementPresenter {

    private final Context context;
    private final AnnouncementPresenterListener mListener;
    private BaseApiService mApiService;
    private String token = Hawk.get(Constant.Key.KEY_TOKEN);

    public AnnouncementPresenter(Context context, AnnouncementPresenterListener mListener, BaseApiService mApiService) {
        this.context = context;
        this.mListener = mListener;
        this.mApiService = mApiService;
    }

    public interface AnnouncementPresenterListener{
        void getAnnouncement(List<Result> news);
    }

    public void getAnnouncementData(){
        mApiService.getAnnouncement(token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data->
                        {
                            if (data.getSuccess() == 1) mListener.getAnnouncement(data.getData().getResult());
                            else mListener.getAnnouncement(null);
                        },throwable ->
                        {
                            mListener.getAnnouncement(null);
                            Toast.makeText(context,"Tidak Ada Koneksi Internet",Toast.LENGTH_SHORT).show();
                        }
                );
    }
}
