package ortu.pinisi.io.ui.fragment.absent;

import android.content.Context;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

import java.util.HashMap;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ortu.pinisi.io.data.apihelper.BaseApiService;
import ortu.pinisi.io.model.studentabsen.Result;
import ortu.pinisi.io.model.studentabsen.Student;
import ortu.pinisi.io.model.studentfinalmark.Quize;
import ortu.pinisi.io.model.Constant;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mfahm on 1/16/2018.
 */

public class AbsentPresenter {

    private final Context context;
    private final AbsentPresenterListener mListener;
    private final BaseApiService mApiService;

    public AbsentPresenter(Context context, AbsentPresenterListener mListener, BaseApiService mApiService) {
        this.context = context;
        this.mListener = mListener;
        this.mApiService = mApiService;
    }

    public interface AbsentPresenterListener{
        void getAbsent(List<Result> absent);

        void getFinalMark(List<Quize> quiz, boolean error);
    }

    public void getAbsentData(){
        String user_id = Hawk.get(Constant.Key.KEY_USER_ID);
        String child_id = Hawk.get(Constant.Key.KEY_CHILD_ID);
//        String lesson_id = lesson.get(SessionManager.KEY_LESSON_ID);
        String token = Hawk.get(Constant.Key.KEY_TOKEN);

        mApiService.getData(user_id,child_id,"0","100",token)
                .enqueue(new Callback<Student>() {
                    @Override
                    public void onResponse(Call<Student> call, Response<Student> response) {
                        if (response.body() != null){
                            if (response.body().getSuccess() == 1){
                                mListener.getAbsent(response.body().getData().getResult());
                            }else{

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Student> call, Throwable t) {

                    }
                });
    }

    public void getFinalMarkData(){
        //get Data from pref
        String child_id = Hawk.get(Constant.Key.KEY_CHILD_ID);
        String token = Hawk.get(Constant.Key.KEY_TOKEN);
        String semester = Hawk.get(Constant.Key.KEY_SEMESTER);
        String tahun = Hawk.get(Constant.Key.KEY_TAHUN_AJARAN);

        mApiService.getFinalMark(child_id, tahun, semester, token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data->
                        {
                            if (data.getSuccess() == 1) mListener.getFinalMark(data.getData().getResult().getQuizes(),false);
                            else mListener.getFinalMark(null, true);
                        },throwable ->
                        {
                            mListener.getFinalMark(null, true);
                            Toast.makeText(context,"tidak ada data ujian , silahkan coba lagi",Toast.LENGTH_LONG).show();
                        }
                );
    }
}
