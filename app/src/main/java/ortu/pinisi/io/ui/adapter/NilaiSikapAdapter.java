package ortu.pinisi.io.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ortu.pinisi.io.R;

/**
 * Created by mfahm on 1/30/2018.
 */

public class NilaiSikapAdapter extends RecyclerView.Adapter<NilaiSikapAdapter.ViewHolder>{
    private final Context context;
    List<String> list;

    public NilaiSikapAdapter(Context context, List<String> list) {
        this.list = new ArrayList<>();
        this.context = context;
        this.list = list;
    }

    @Override
    public NilaiSikapAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_sikap, null);

        itemLayoutView.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.
                MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));

        // create ViewHolder
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NilaiSikapAdapter.ViewHolder holder, int position) {
        holder.numberText.setText(""+(position+1));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView numberText;

        public ViewHolder(View itemView) {
            super(itemView);

            numberText = (TextView) itemView.findViewById(R.id.textNumber);
        }
    }
}
