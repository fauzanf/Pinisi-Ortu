package ortu.pinisi.io.ui.adapter;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ortu.pinisi.io.R;
import ortu.pinisi.io.model.studentabsen.Result;
import ortu.pinisi.io.ui.activity.main.MainButtomNav;
import ortu.pinisi.io.ui.fragment.absent.AbsenAnakFragment;
import ortu.pinisi.io.ui.fragment.ComingSoonFragment;

/**
 * Created by Ojan on 7/13/2017.
 */

public class SiswaAdapter extends RecyclerView.Adapter<SiswaAdapter.ViewHolder> {

    static List<Result> dblist;
    Context context;

    public SiswaAdapter(Context context, List<Result> dblist){
        this.dblist = new ArrayList<Result>();
        this.context = context;
        this.dblist = dblist;
    }

    @Override
    public SiswaAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_siswa, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SiswaAdapter.ViewHolder holder, int position) {

        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd MMMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(dblist.get(position).getTanggal());
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        String nama_siswa = dblist.get(position).getUser().getName();
        String kelas_siswa = dblist.get(position).getTanggal();
        String sekolah_siswa = "";
        String status_siswa = dblist.get(position).getAbsen();

        holder.name.setText(nama_siswa);
        holder.kelas.setText(" ");
        holder.sekolah.setText(str);
        if(status_siswa.equals("hadir")){
            holder.status.setText(status_siswa);
            holder.status.setTextColor(Color.parseColor("#FF0BE212"));
        }else if(status_siswa.equals("sakit")){
            holder.status.setText(status_siswa);
            holder.status.setTextColor(Color.parseColor("#FF0065D9"));
        }else if(status_siswa.equals("izin")){
            holder.status.setText(status_siswa);
            holder.status.setTextColor(Color.parseColor("#FF939393"));
        }else{
            holder.status.setText(status_siswa);
            holder.status.setTextColor(Color.parseColor("#FFE60000"));
        }


    }

    @Override
    public int getItemCount() {
        return dblist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name,kelas,sekolah,status;

        public ViewHolder(final View itemLayoutView) {
            super(itemLayoutView);

            name = (TextView) itemLayoutView.findViewById(R.id.tv_SiswaName);
            kelas = (TextView) itemLayoutView.findViewById(R.id.tv_KelasName);
            sekolah = (TextView) itemLayoutView.findViewById(R.id.tv_SekolahName);
            status = (TextView) itemLayoutView.findViewById(R.id.tv_Status);

            itemLayoutView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final int a = getAdapterPosition();
                    if(dblist.size()>=0){
//                        final int id_anak = dblist.get(a).getId_anak();
                        final String namaa = dblist.get(a).getUser().getName();
                        final String kelas = "";
                        final String sekolah = "";

                        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(context);
                        final View mView = layoutInflaterAndroid.inflate(R.layout.detail_profile, null);
                        final AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(context);
                        alertDialogBuilderUserInput.setView(mView);

                        final LinearLayout absenn = (LinearLayout)mView.findViewById(R.id.absen);
                        final LinearLayout nilaii = (LinearLayout)mView.findViewById(R.id.nilai);
                        final TextView nama_profile = (TextView)mView.findViewById(R.id.namaProfile);
                        final TextView kelas_profile = (TextView)mView.findViewById(R.id.kelasProfile);
                        final TextView sekolah_profile = (TextView)mView.findViewById(R.id.sekolahProfile);

                        nama_profile.setText(namaa);
                        kelas_profile.setText(kelas);
                        sekolah_profile.setText(sekolah);

                        final AlertDialog ad = alertDialogBuilderUserInput.show();

                        absenn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Fragment fragment = new AbsenAnakFragment();
                                Bundle nama = new Bundle();
                                nama.putString("namasiswa",namaa);
                                fragment.setArguments(nama);

                                FragmentManager fragmentManager = ((MainButtomNav)context).getSupportFragmentManager();
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                                fragmentTransaction.replace(R.id.content, fragment);
                                fragmentTransaction.addToBackStack(null);
                                fragmentTransaction.commit();
                                ad.dismiss();

                            }
                        });

                        nilaii.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Fragment fragment = new ComingSoonFragment();
                                Bundle nama = new Bundle();
                                nama.putString("namasiswa",namaa);
                                fragment.setArguments(nama);

                                FragmentManager fragmentManager = ((MainButtomNav)context).getSupportFragmentManager();
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction.replace(R.id.content, fragment);
                                fragmentTransaction.addToBackStack(null);
                                fragmentTransaction.commit();
                                ad.dismiss();
                            }
                        });

//                        AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
//                        alertDialogAndroid.show();
                    }
                }
            });

        }

    }
}
