package ortu.pinisi.io.ui.fragment.home;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ortu.pinisi.io.data.apihelper.BaseApiService;
import ortu.pinisi.io.model.studentabsen.Student;
import ortu.pinisi.io.model.studentfinalmark.Quize;
import ortu.pinisi.io.model.Constant;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mfahm on 1/16/2018.
 */


public class ListMainPresenter {

    private final Context context;
    private final BaseApiService mApiService;
    private final ListMainListener mListener;
    private String token = Hawk.get(Constant.Key.KEY_TOKEN);
    private String child_id = Hawk.get(Constant.Key.KEY_CHILD_ID);
    private String semester = Hawk.get(Constant.Key.KEY_SEMESTER);
    private String tahun = Hawk.get(Constant.Key.KEY_TAHUN_AJARAN);

    public ListMainPresenter(Context context, BaseApiService mApiService, ListMainListener mListener) {
        this.context = context;
        this.mApiService = mApiService;
        this.mListener = mListener;
    }

    public interface ListMainListener{
        void examScoresRespone(List<Quize> results);

        void absentLessonRespone(List<ortu.pinisi.io.model.absent.Result> results);
    }

    public void getAbsentData(String date){
        mApiService.getData(date,child_id,"0","100",token)
                .enqueue(new Callback<Student>() {
                    @Override
                    public void onResponse(Call<Student> call, Response<Student> response) {
                        if (response.body() != null){
                            if (response.body().getSuccess() == 1){
//                                mListener.getAbsent(response.body().getData().getResult());
                            }else{
                                Log.d("Error Absent", "No Data Found");
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Student> call, Throwable t) {
                        try {
                            throw  new InterruptedException("Error Internet Connection");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    public void getScores(String date){
        mApiService.getStudentQuize(child_id, tahun, semester, token,date)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data->
                        {
                            if (data.getSuccess() == 1) mListener.examScoresRespone(data.getData().getResult().getQuizes());
                        },throwable -> Log.e("errorQuize", "No Data found")
                );
    }

    public void getAbsentLesson(String date){
        mApiService.getAbsent(token,semester,tahun,child_id,date)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data->
                        {
                            if (data.getSuccess() == 1) mListener.absentLessonRespone(data.getData().getResult());
                        },throwable -> Toast.makeText(context,"Tidak ada koneksi internet",Toast.LENGTH_SHORT).show()
                );
    }
}
