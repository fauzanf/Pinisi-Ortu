package ortu.pinisi.io.ui.activity.announcementActivity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.orhanobut.hawk.Hawk;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ortu.pinisi.io.R;
import ortu.pinisi.io.model.Constant;

public class AnnouncementActivity extends AppCompatActivity {

    @BindView(R.id.contentAnnouncement)
    WebView Content;

    String content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_announcement);

        // inizialitation view

        content = Hawk.get(Constant.Key.KEY_CONTENT);

        ButterKnife.bind(this);

        WebSettings settings = Content.getSettings();
        settings.setDefaultTextEncodingName("utf-8");

        Content.loadData(content, "text/html; charset=utf-8", "utf-8");

    }

    //image view clicked
    @OnClick(R.id.back)
    public void back(){
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    //on back pressed
    @Override
    public void onBackPressed(){
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}
