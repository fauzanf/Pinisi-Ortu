package ortu.pinisi.io.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ortu.pinisi.io.R;
import ortu.pinisi.io.model.studentfinalmark.Quize;

/**
 * Created by mfahm on 1/30/2018.
 */

public class NilaiPengetahuanAdapter extends RecyclerView.Adapter<NilaiPengetahuanAdapter.ViewHolder> {

    private final Context context;
    List<Quize> list;

    public NilaiPengetahuanAdapter(Context context, List<Quize> list) {
        this.list = new ArrayList<>();
        this.context = context;
        this.list = list;
    }

    @Override
    public NilaiPengetahuanAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_nilai, null);

        itemLayoutView.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.
                MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));

        // create ViewHolder
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NilaiPengetahuanAdapter.ViewHolder holder, int position) {
        String inputPattern = "yyyy-MM-dd hh:mm:ss";
        String outputPattern = "dd MMMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        String currentDateandTime = outputFormat.format(new Date());
        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(list.get(position).getCreated_at());
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

//        if (str.equals(currentDateandTime)){
            holder.numberText.setText(""+(position+1));
            holder.mapelText.setText(list.get(position).getTitle());
            holder.valueText.setText(list.get(position).getScore());
            if (Integer.parseInt(list.get(position).getScore()) >= 75){
                holder.valueText.setTextColor(Color.parseColor("#2ecc71"));
            }else{
                holder.valueText.setTextColor(Color.parseColor("#ff7f00"));
            }
        /*}else{

        }*/
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView numberText,mapelText,valueText;

        public ViewHolder(View itemView) {
            super(itemView);

            numberText = (TextView) itemView.findViewById(R.id.textNumber);
            mapelText = (TextView) itemView.findViewById(R.id.textMapel);
            valueText = (TextView) itemView.findViewById(R.id.textValue);
        }
    }
}
