package ortu.pinisi.io.ui.activity.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.orhanobut.hawk.Hawk;
import com.orhanobut.hawk.NoEncryption;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ortu.pinisi.io.R;
import ortu.pinisi.io.data.apihelper.BaseApiService;
import ortu.pinisi.io.data.apihelper.UtilsApi;
import ortu.pinisi.io.model.Constant;
import ortu.pinisi.io.model.studentoption.Result;
import ortu.pinisi.io.ui.activity.login.LoginActivity;
import ortu.pinisi.io.ui.fragment.announcement.AnnouncementFragment;
import ortu.pinisi.io.ui.fragment.examScores.NilaiAnakFragment;
import ortu.pinisi.io.ui.fragment.home.ListMainFragment;
import ortu.pinisi.io.utils.util;

public class MainButtomNav extends AppCompatActivity implements MainPresenter.MainPresenterListener{

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.navigation)
    BottomNavigationView navigation;

    private TextView mTextMessage;
    private boolean doubleBackToExitPressedOnce;
    private Handler mHandler = new Handler();
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    MainPresenter mainPresenter;
    Context mContext;
    BaseApiService mApiService;

    String url;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
                Fragment fragment;
                FragmentManager fm = getSupportFragmentManager();

                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        fragment = new AnnouncementFragment();


                        fm.beginTransaction()
                                .replace(R.id.content, fragment)
                                .addToBackStack(null)
                                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
                                .commit();

    //                    mTextMessage.setText(R.string.title_home);
                        return true;
                    case R.id.navigation_dashboard:
                        fragment = new ListMainFragment();

                        fm.beginTransaction()
                                .replace(R.id.content, fragment)
                                .addToBackStack(null)
                                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
                                .commit();
    //                    mTextMessage.setText(R.string.title_dashboard);
                        return true;
                    case R.id.navigation_notifications:
                        fragment = new NilaiAnakFragment();

                        fm.beginTransaction()
                                .replace(R.id.content, fragment)
                                .addToBackStack(null)
                                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
                                .commit();
    //                    mTextMessage.setText(R.string.title_notifications);
                        return true;

                    default:
                        fragment = new AnnouncementFragment();

                        fm.beginTransaction()
                                .replace(R.id.content, fragment)
                                .addToBackStack(null)
                                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
                                .commit();
                }


                //set actionbar title
                setTitle(item.getTitle());

                return true;
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_buttom_nav);

        ButterKnife.bind(this);

        //build hawk
        Hawk.init(MainButtomNav.this)
                .setEncryption(new NoEncryption())
                .build();

        url = Hawk.get(Constant.Key.KEY_URL);

        setSupportActionBar(toolbar);

        Fragment fragment = new ListMainFragment();

        FragmentManager fm = getSupportFragmentManager();
            fm.beginTransaction()
                .replace(R.id.content, fragment)
                .addToBackStack(null)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
                .commit();

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) navigation.getLayoutParams();

        layoutParams.setBehavior(new BottomNavigationViewBehavior());

        mContext = MainButtomNav.this;
        mApiService = UtilsApi.getAPIService(MainButtomNav.this, url); // meng-init yang ada di package apihelper

        //get option data
        mainPresenter = new MainPresenter(mContext,mApiService, this);
        mainPresenter.getOptionDatas();

        util.setMixPanel(mContext, "MainActivity");
    }

    @Override
    public void getOption(List<Result> option) {
        String semester = null, tahun = null, school;
        for (Result results : option){
            if (results.getKeyConfig().equals("school_name")){
                school = results.getValue();
                Hawk.put(Constant.Key.KEY_SCHOOL, school);
                Log.d("School", school);
            }

            if (results.getKeyConfig().equals("semester")) {
                semester = results.getValue();
                Hawk.put(Constant.Key.KEY_SEMESTER,semester);
                Log.d("semester", semester);
            }

            if (results.getKeyConfig().equals("tahun_ajaran")) {
                tahun = results.getValue();
                Hawk.put(Constant.Key.KEY_TAHUN_AJARAN,tahun);
                Log.d("tahun", tahun);
            }
        }
    }

    private final Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            doubleBackToExitPressedOnce = false;
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mHandler != null) {
            mHandler.removeCallbacks(mRunnable);
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            finishAffinity();
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Tekan \"Kembali\" untuk keluar", Toast.LENGTH_SHORT).show();

        mHandler.postDelayed(mRunnable, 3000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout) {
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(MainButtomNav.this);
            builder.setTitle("Logout");
            builder.setMessage("Apakah anda yakin untuk logout");
            builder.setPositiveButton("Ya", (dialogInterface, i) -> {

                Intent intent = new Intent(MainButtomNav.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

                FirebaseAuth.getInstance().signOut();
                dialogInterface.dismiss();
                Hawk.deleteAll();
                finish();

            });
            builder.setNegativeButton("Tidak", (dialog, which) -> dialog.dismiss());

            android.app.AlertDialog dialog = builder.create();
            dialog.show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
