package ortu.pinisi.io.ui.fragment.examScores;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import ortu.pinisi.io.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class Nilai_fragment_home extends Fragment {

    int[] nilai_keterampilan = new int[]{80,80,90,75,80,95};
    List nilai = new ArrayList();
    List nilai_ket = new ArrayList();
    List listpelajaran = new ArrayList();
    TextView nama_siswa;
    String[] semes = new String[]{"1","2","3","4","5","6"};
    Spinner semester;

    public Nilai_fragment_home() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_nilai_fragment_home, container, false);

        String value = getArguments().getString("namasiswa");
        nama_siswa = (TextView)view.findViewById(R.id.namasiswa);
        semester = (Spinner)view.findViewById(R.id.s_semester);

        ArrayAdapter<String> adapterSemester = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,semes);
        semester.setAdapter(adapterSemester);

        nama_siswa.setText("Nama Siswa : "+value);


        listpelajaran.add("Pendidikan Agama dan Budi Pekerti");
        listpelajaran.add("Ilmu Pengetahuan Alam");
        listpelajaran.add("Bahasa Indonesia");
        listpelajaran.add("Bahasa Inggris");
        listpelajaran.add("Matematika");
        listpelajaran.add("Seni Budaya");

        nilai.add(80);
        nilai.add(85);
        nilai.add(90);
        nilai.add(75);
        nilai.add(88);
        nilai.add(85);

        nilai_ket.add(88);
        nilai_ket.add(85);
        nilai_ket.add(86);
        nilai_ket.add(80);
        nilai_ket.add(90);
        nilai_ket.add(77);
        init(view);

        return view;
    }

    public void init(View view) {
        TableLayout stk = (TableLayout) view.findViewById(R.id.table_main);
        TableRow tbrow0 = new TableRow(getContext());
        TextView tv1 = new TextView(getContext());
        tv1.setText(" Pelajaran ");
        tv1.setTextAppearance(getContext(), android.R.style.TextAppearance_DeviceDefault_Medium);
        tv1.setTextColor(Color.BLACK);
        tv1.setBackgroundResource(R.drawable.cell_shape);
        tbrow0.addView(tv1);

        TextView tv2 = new TextView(getContext());
        tv2.setText(" Pengetahuan ");
        tv2.setTextAppearance(getContext(), android.R.style.TextAppearance_DeviceDefault_Medium);
        tv2.setTextColor(Color.BLACK);
        tv2.setBackgroundResource(R.drawable.cell_shape);
        tbrow0.addView(tv2);

        TextView tv3 = new TextView(getContext());
        tv3.setText(" Keterampilan ");
        tv3.setTextAppearance(getContext(), android.R.style.TextAppearance_DeviceDefault_Medium);
        tv3.setTextColor(Color.BLACK);
        tv3.setBackgroundResource(R.drawable.cell_shape);
        tbrow0.addView(tv3);

        stk.addView(tbrow0);

        TableLayout tb = (TableLayout) view.findViewById(R.id.table_main1);
        TableRow tbrow1 = new TableRow(getContext());
        TextView pelajaran = new TextView(getContext());
        pelajaran.setText(" Pelajaran ");
        pelajaran.setTextAppearance(getContext(), android.R.style.TextAppearance_DeviceDefault_Medium);
        pelajaran.setTextColor(Color.BLACK);
        pelajaran.setBackgroundResource(R.drawable.cell_shape);
        tbrow1.addView(pelajaran);

        TextView deskripsi = new TextView(getContext());
        deskripsi.setText(" Deskripsi ");
        deskripsi.setTextAppearance(getContext(), android.R.style.TextAppearance_DeviceDefault_Medium);
        deskripsi.setTextColor(Color.BLACK);
        deskripsi.setBackgroundResource(R.drawable.cell_shape);
        tbrow1.addView(deskripsi);

        tb.addView(tbrow1);

        for (int i = 0; i < nilai.size(); i++) {

            TableRow tbrow = new TableRow(getContext());
            TextView t2v = new TextView(getContext());
            t2v.setText(" " + listpelajaran.get(i).toString()+" ");
            t2v.setTextColor(Color.BLACK);
//            t2v.setGravity(Gravity.CENTER);
            t2v.setTextAppearance(getContext(), android.R.style.TextAppearance_DeviceDefault_Medium);
            t2v.setBackgroundResource(R.drawable.cell_shape);
            tbrow.addView(t2v);

            TextView t3v = new TextView(getContext());
            t3v.setText(""+nilai.get(i).toString());
            t3v.setTextColor(Color.BLACK);
            t3v.setGravity(Gravity.CENTER);
            t3v.setTextAppearance(getContext(), android.R.style.TextAppearance_DeviceDefault_Medium);
            t3v.setBackgroundResource(R.drawable.cell_shape);
            tbrow.addView(t3v);

            TextView t4v = new TextView(getContext());
            t4v.setText(""+nilai_ket.get(i).toString());
            t4v.setTextColor(Color.BLACK);
            t4v.setGravity(Gravity.CENTER);
            t4v.setTextAppearance(getContext(), android.R.style.TextAppearance_DeviceDefault_Medium);
            t4v.setBackgroundResource(R.drawable.cell_shape);
            tbrow.addView(t4v);

            stk.addView(tbrow);


        }

        for (int i = 0; i < nilai.size(); i++) {

            TableRow tbroww = new TableRow(getContext());
            TextView t2v = new TextView(getContext());
            t2v.setText(" " + listpelajaran.get(i).toString()+ " ");
            t2v.setTextColor(Color.BLACK);
//            t2v.setGravity(Gravity.CENTER);
            t2v.setTextAppearance(getContext(), android.R.style.TextAppearance_DeviceDefault_Medium);
            t2v.setBackgroundResource(R.drawable.cell_shape);
            tbroww.addView(t2v);

            TextView t3v = new TextView(getContext());
            t3v.setText(" Deskripsi ");
            t3v.setTextColor(Color.BLACK);
            t3v.setGravity(Gravity.CENTER);
            t3v.setTextAppearance(getContext(), android.R.style.TextAppearance_DeviceDefault_Medium);
            t3v.setBackgroundResource(R.drawable.cell_shape);
            tbroww.addView(t3v);


            tb.addView(tbroww);


        }

    }

}
