package ortu.pinisi.io.ui.fragment.absent;

import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.orhanobut.hawk.Hawk;

import ortu.library.CompactCalendarView;
import ortu.library.domain.Event;
import ortu.pinisi.io.data.apihelper.BaseApiService;
import ortu.pinisi.io.data.apihelper.UtilsApi;
import ortu.pinisi.io.R;
import ortu.pinisi.io.model.studentabsen.Result;
import ortu.pinisi.io.model.studentabsen.Student;
import ortu.pinisi.io.model.Constant;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class fragment__absen_home extends Fragment {

    TextView nama_siswa;
    TextView keterangan, ket_absen;
    List<String> dblist;
    TextView month, studentName;
    String namasiswa;
    CompactCalendarView compactCalendarView;
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy", Locale.getDefault());
    String[] lisanak = new String[]{"Muhammad", "Fauzan", "Fahmuddin"};
    private NotificationManager mNotificationManager;
    private int notificationID = 100;
    private int totalMessages = 0;
    Context mContext;
    BaseApiService mApiService;
    List<String> listAnak;
    SimpleDateFormat f, Tgl;
    LinearLayout p_month, n_month;
    Date Now;

    String url;


    public fragment__absen_home() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment__absen_home, container, false);
//Initialize CustomCalendarView from layout
        url = Hawk.get(Constant.Key.KEY_URL);

        nama_siswa = (TextView)view.findViewById(R.id.namasiswa);
        String value = getArguments().getString("namasiswa");
        nama_siswa.setText("Nama Siswa : "+value);

        compactCalendarView = (CompactCalendarView) view.findViewById(R.id.compactcalendar_view);
        //keterangan = (TextView) view.findViewById(R.id.ket);
        p_month = (LinearLayout) view.findViewById(R.id.pmonth);
        n_month = (LinearLayout) view.findViewById(R.id.nmonth);
        month = (TextView) view.findViewById(R.id.bulan);
        ket_absen = (TextView) view.findViewById(R.id.keteranganabsen);

        mContext = getContext();
        mApiService = UtilsApi.getAPIService(getContext(),url); // meng-init yang ada di package apihelper


        p_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compactCalendarView.showPreviousMonth();

            }
        });

        n_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compactCalendarView.showNextMonth();
            }
        });

        month.setText(dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()));

        compactCalendarView.setFirstDayOfWeek(Calendar.MONDAY);

        getStudentData();

//        Event ev2 = new Event(Color.YELLOW, 1499223651000L, "Sakit");
//        compactCalendarView.addEvent(ev2);
//
//        Event ev3 = new Event(Color.LTGRAY, 1499310051000L, "Izin");
//        compactCalendarView.addEvent(ev3);
//
//        Event ev4 = new Event(Color.RED, 1499396451000L, "Alfa");
//        compactCalendarView.addEvent(ev4);
//
//        Event ev5 = new Event(Color.RED, 1499706000000L, "Hadir");
//        compactCalendarView.addEvent(ev5);

//        if(Calendar.DAY_OF_MONTH == day){
//
//        }

//        ket_absen.setText(""+dateString);

//        compactCalendarView.setCurrentSelectedDayBackgroundColor(0);
//
//        compactCalendarView.setCurrentDayBackgroundColor(0);


        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                month.setText(dateFormatForMonth.format(dateClicked));
                List<Event> showEvent = compactCalendarView.getEvents(dateClicked);
                if (showEvent.size() >= 1) {

                    String data = showEvent.get(0).toString();
                    String[] items = data.split(",");
                    String select = "" + items[2];
                    String fix = select.replace("data=", "");
                    String fixx = fix.replace("}", "");


//                Toast.makeText(getContext(),""+showEvent,Toast.LENGTH_LONG).show();
                    if (fixx.equals(" Alfa")) {
                        ket_absen.setText("Tidak ada Keterangan");
                    } else if (fixx.equals(" Hadir")) {
                        ket_absen.setText("Hadir");
                    } else {
                        ket_absen.setText("");
                    }
                    //keterangan.setText(""+fixx);
                }
                ;//else {keterangan.setText("");}

            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                month.setText(dateFormatForMonth.format(firstDayOfNewMonth));
            }
        });


//        CalendarView cv = (CalendarView)view.findViewById(R.id.calendarAnak);
//
//        long da =System.currentTimeMillis();
//        cv.setDate(da,true,true);

        //Initialize CustomCalendarView from layout

        return view;
    }

    public void getStudentData() {
        String user_id = Hawk.get(Constant.Key.KEY_USER_ID);
        String child_id = Hawk.get(Constant.Key.KEY_CHILD_ID);
//        String lesson_id = lesson.get(SessionManager.KEY_LESSON_ID);
        String token = Hawk.get(Constant.Key.KEY_TOKEN);

        listAnak = new ArrayList<String>();

        mApiService.getData(user_id,child_id, "0", "100", token)
                .enqueue(new Callback<Student>() {
                    @Override
                    public void onResponse(Call<Student> call, Response<Student> response) {
//                        Toast.makeText(getContext(),response.body().getData().getResult().get(0).getId()+"",Toast.LENGTH_LONG).show();
                        p_month.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                compactCalendarView.showPreviousMonth();

                            }
                        });

                        n_month.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                compactCalendarView.showNextMonth();
                            }
                        });

                        month.setText(dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()));

                        compactCalendarView.setFirstDayOfWeek(Calendar.MONDAY);

                        long timeInMilliseconds = 0, timeInMilliNow = 0;
//                        String currentDateandTime = Tgl.format(new Date());
                        String dateStudent = null;
                        int color, colorNow;

                        for (Result result : response.body().getData().getResult()) {
                            //                                Date mDate = f.parse(result.getTanggal());
//                                timeInMilliseconds = mDate.getTime();
                            Log.d("timeinmiles", timeInMilliseconds + "");
//                                Now = f.parse(response.body().getData().getResult().get(0).getTanggal());
//                                dateStudent = Tgl.format(Now);

                            if (result.getAbsen().equals("hadir")) {
                                color = Color.parseColor("#00bbd3");
                            } else if (result.getAbsen().equals("sakit")) {
                                color = Color.YELLOW;

                            } else if (result.getAbsen().equals("izin")) {
                                color = Color.LTGRAY;
                            } else {
                                color = Color.RED;
                            }


                            Event ev = new Event(color, timeInMilliseconds, result.getAbsen());
                            compactCalendarView.addEvent(ev);


//                                Log.d("samedate", dateStudent + "," + currentDateandTime);

                            colorNow = Color.DKGRAY;
                            compactCalendarView.setCurrentDayBackgroundColor(colorNow);
                            compactCalendarView.setCurrentSelectedDayBackgroundColor(colorNow);

                        }
                    }

                    @Override
                    public void onFailure(Call<Student> call, Throwable t) {
                        Toast.makeText(getContext(), t.toString(), Toast.LENGTH_LONG).show();
                    }
                });

    }

    @Override
    public void onResume() {
        super.onResume();
        month.setText(dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()));
    }


}
