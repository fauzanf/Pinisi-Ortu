package ortu.pinisi.io.ui.activity.main;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.orhanobut.hawk.Hawk;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ortu.pinisi.io.data.apihelper.BaseApiService;
import ortu.pinisi.io.model.studentoption.Result;
import ortu.pinisi.io.model.Constant;

/**
 * Created by mfahm on 1/16/2018.
 */
@SuppressLint("CheckResult")
public class MainPresenter {

    private final Context context;
    private BaseApiService mApiService;
    private final MainPresenterListener mListener;
    private String token = Hawk.get(Constant.Key.KEY_TOKEN);

    public MainPresenter(Context context, BaseApiService mApiService, MainPresenterListener mListener) {
        this.context = context;
        this.mApiService = mApiService;
        this.mListener = mListener;
    }

    public interface MainPresenterListener{
        void getOption(List<Result> option);
    }

    public void getOptionDatas(){
        mApiService.getOption(token,"option")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data->
                        {
                            if (data.getSuccess() == 1) mListener.getOption(data.getData().getResult());
                        },throwable -> Log.e("errorOption","Tidak ada koneksi internet")
                );
    }

}
