package ortu.pinisi.io.ui.fragment.examScores;

import android.content.Context;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ortu.pinisi.io.data.apihelper.BaseApiService;
import ortu.pinisi.io.model.studentfinalmark.Quize;
import ortu.pinisi.io.model.Constant;

/**
 * Created by mfahm on 1/15/2018.
 */

public class examScoresPresenter {

    private final Context context;
    private final ExamScoresPresenterListener mListener;
    private BaseApiService mApiService;
    private String child_id = Hawk.get(Constant.Key.KEY_CHILD_ID);
    private String token = Hawk.get(Constant.Key.KEY_TOKEN);
    private String semester = Hawk.get(Constant.Key.KEY_SEMESTER);
    private String tahun = Hawk.get(Constant.Key.KEY_TAHUN_AJARAN);

    public examScoresPresenter(Context context, ExamScoresPresenterListener mListener, BaseApiService mApiService) {
        this.context = context;
        this.mListener = mListener;
        this.mApiService = mApiService;
    }

    public interface ExamScoresPresenterListener{
        void examScoresRespone(List<Quize> quiz);
    }

    public void getScores(){
        mApiService.getFinalMark(child_id, tahun, semester, token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data->
                        {
                            if (data.getSuccess() == 1) mListener.examScoresRespone(data.getData().getResult().getQuizes());
                            else mListener.examScoresRespone(null);
                        },throwable ->
                        {
                            mListener.examScoresRespone(null);
                            Toast.makeText(context,"Tidak Ada Koneksi Internet",Toast.LENGTH_SHORT).show();
                        }
                );
    }

}
