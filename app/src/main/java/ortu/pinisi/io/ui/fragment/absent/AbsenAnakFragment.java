package ortu.pinisi.io.ui.fragment.absent;

import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.orhanobut.hawk.Hawk;
import com.orhanobut.hawk.NoEncryption;

import butterknife.BindView;
import butterknife.ButterKnife;
import ortu.library.CompactCalendarView;
import ortu.library.domain.Event;
import ortu.pinisi.io.data.apihelper.BaseApiService;
import ortu.pinisi.io.data.apihelper.UtilsApi;
import ortu.pinisi.io.R;
import ortu.pinisi.io.model.studentabsen.Result;
import ortu.pinisi.io.model.studentfinalmark.Quize;
import ortu.pinisi.io.model.Constant;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class AbsenAnakFragment extends Fragment implements AbsentPresenter.AbsentPresenterListener{

    @BindView(R.id.compactcalendar_view)
    CompactCalendarView compactCalendarView;

    @BindView(R.id.pmonth)
    LinearLayout p_month;

    @BindView(R.id.nmonth)
    LinearLayout n_month;

    @BindView(R.id.progress)
    ProgressBar progress;

    @BindView(R.id.hscrll1)
    HorizontalScrollView hrzScroll;

    @BindView(R.id.namaSiswa)
    TextView studentName;

    @BindView(R.id.bulan)
    TextView month;

    @BindView(R.id.keteranganabsen)
    TextView ket_absen;

    @BindView(R.id.ketText)
    TextView ketText;

    @BindView(R.id.nilaiText)
    TextView scoreText;

    @BindView(R.id.textNothingExam)
    TextView textExam;

    @BindView(R.id.table_main)
    TableLayout stk;

    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy", Locale.getDefault());
    private NotificationManager mNotificationManager;
    private int notificationID = 100;
    private int totalMessages = 0;

    List<String> nilai = new ArrayList<>();
    List<String> dateExam = new ArrayList<>();
    List nilai_ket = new ArrayList();
    List<String> listpelajaran = new ArrayList<>();
    List<String> dblist;
    List<String> listAnak;
    String[] lisanak = new String[]{"Muhammad", "Fauzan", "Fahmuddin"};

    String namasiswa, dateNow, url, monthNow;
    AbsentPresenter absentPresenter;

    Context mContext;
    BaseApiService mApiService;
    SimpleDateFormat f, Tgl, monthFormat;
    Date Now;


    public AbsenAnakFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_absen_anak, container, false);

        Hawk.init(getContext())
                .setEncryption(new NoEncryption())
                .build();

        url = Hawk.get(Constant.Key.KEY_URL);

        ButterKnife.bind(this, view);

        String string_date = "2017-07-20";

        f = new SimpleDateFormat("yyyy-MM-dd");
        Tgl = new SimpleDateFormat("dd");
        monthFormat = new SimpleDateFormat("MM");

        dateNow = f.format(new Date());
        monthNow = monthFormat.format(new Date());

        Date d = null;
        try {
            d = f.parse(string_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long milliseconds = d.getTime();
//        Toast.makeText(getContext(),""+milliseconds,Toast.LENGTH_LONG).show();

        //keterangan = (TextView) view.findViewById(R.id.ket);

        mContext = getContext();
        mApiService = UtilsApi.getAPIService(getContext(), url); // meng-init yang ada di package apihelper

        //get Data from API
        absentPresenter = new AbsentPresenter(getContext(),this,mApiService);
        absentPresenter.getAbsentData();
        absentPresenter.getFinalMarkData();

        p_month.setOnClickListener(v -> {
            compactCalendarView.showPreviousMonth();
            absentPresenter.getFinalMarkData();
        });

        n_month.setOnClickListener(v -> {
            compactCalendarView.showNextMonth();
            absentPresenter.getFinalMarkData();
        });

        month.setText(dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()));

        compactCalendarView.setFirstDayOfWeek(Calendar.MONDAY);

        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                month.setText(dateFormatForMonth.format(dateClicked));
                List<Event> showEvent = compactCalendarView.getEvents(dateClicked);
                if (showEvent.size() >= 1) {

                    String data = showEvent.get(0).toString();
                    String[] items = data.split(",");
                    String select = "" + items[2];
                    String fix = select.replace("data=", "");
                    String fixx = fix.replace("}", "");
                    String[] eventDesc = fixx.split("/");

                    Log.d("eventCalendar", eventDesc[0] + "");

//                Toast.makeText(getContext(),""+showEvent,Toast.LENGTH_LONG).show();
                    if (eventDesc[0].equals(" Alfa")) {
                        ket_absen.setText("Tidak ada Keterangan");
                        compactCalendarView.setCurrentSelectedDayBackgroundColor(Color.RED);
                        compactCalendarView.setCurrentDayBackgroundColor(Color.RED);
                    } else if (eventDesc[0].equals(" Hadir")) {
                        ketText.setVisibility(View.GONE);
                        compactCalendarView.setCurrentSelectedDayBackgroundColor(Color.parseColor("#00bbd3"));
                        compactCalendarView.setCurrentDayBackgroundColor(Color.parseColor("#00bbd3"));
                        ket_absen.setText("");
                    } else if (eventDesc[0].equals(" Izin")) {
                        ketText.setVisibility(View.VISIBLE);
                        compactCalendarView.setCurrentSelectedDayBackgroundColor(Color.GRAY);
                        compactCalendarView.setCurrentDayBackgroundColor(Color.GRAY);
                        ket_absen.setText(eventDesc[1]);
                    } else if (eventDesc[0].equals(" Sakit")) {
                        ketText.setVisibility(View.GONE);
                        compactCalendarView.setCurrentSelectedDayBackgroundColor(Color.YELLOW);
                        compactCalendarView.setCurrentDayBackgroundColor(Color.YELLOW);
                        ket_absen.setText(eventDesc[1]);
                    } else {
                        ketText.setVisibility(View.GONE);
                        compactCalendarView.setCurrentSelectedDayBackgroundColor(Color.GREEN);
                        compactCalendarView.setCurrentDayBackgroundColor(Color.GREEN);
                        ket_absen.setText("");
                    }
                    //keterangan.setText(""+fixx);
                } else {
                    ketText.setVisibility(View.GONE);
                    ket_absen.setText("");
                }
                ;//else {keterangan.setText("");}

            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                month.setText(dateFormatForMonth.format(firstDayOfNewMonth));
            }
        });

        return view;
    }

    public void init() {

        TableRow tbrow0 = new TableRow(getContext());
        TextView tv1 = new TextView(getContext());
        tv1.setText(" Nama Ujian ");
        tv1.setTextAppearance(getContext(), android.R.style.TextAppearance_DeviceDefault_Medium);
        tv1.setTextColor(Color.BLACK);
        tv1.setBackgroundResource(ortu.pinisi.io.R.drawable.cell_shape);
        tbrow0.addView(tv1);

        TextView tv2 = new TextView(getContext());
        tv2.setText(" Nilai ");
        tv2.setTextAppearance(getContext(), android.R.style.TextAppearance_DeviceDefault_Medium);
        tv2.setTextColor(Color.BLACK);
        tv2.setBackgroundResource(ortu.pinisi.io.R.drawable.cell_shape);
        tbrow0.addView(tv2);

        TextView tv3 = new TextView(getContext());
        tv3.setText(" Tanggal Ujian ");
        tv3.setTextAppearance(getContext(), android.R.style.TextAppearance_DeviceDefault_Medium);
        tv3.setTextColor(Color.BLACK);
        tv3.setBackgroundResource(ortu.pinisi.io.R.drawable.cell_shape);
        tbrow0.addView(tv3);

        stk.addView(tbrow0);

        for (int i = 0; i < nilai.size(); i++) {

            TableRow tbrow = new TableRow(getContext());
            TextView t2v = new TextView(getContext());
            t2v.setText(" " + listpelajaran.get(i).toString() + " ");
            t2v.setTextColor(0xFF000000);
//            t2v.setGravity(Gravity.CENTER);
            t2v.setTextAppearance(getContext(), android.R.style.TextAppearance_Medium);
            t2v.setBackgroundResource(ortu.pinisi.io.R.drawable.cell_shape);
            tbrow.addView(t2v);

            TextView t3v = new TextView(getContext());
            t3v.setText("" + nilai.get(i).toString());
            t3v.setTextColor(Color.parseColor("#000000"));
            t3v.setGravity(Gravity.CENTER);
            t3v.setTextAppearance(getContext(), android.R.style.TextAppearance_Medium);
            t3v.setBackgroundResource(ortu.pinisi.io.R.drawable.cell_shape);
            tbrow.addView(t3v);

            TextView t4v = new TextView(getContext());
            t4v.setText("" + dateExam.get(i).toString());
            t4v.setTextColor(Color.parseColor("#000000"));
            t4v.setGravity(Gravity.CENTER);
            t4v.setTextAppearance(getContext(), android.R.style.TextAppearance_Medium);
            t4v.setBackgroundResource(ortu.pinisi.io.R.drawable.cell_shape);
            tbrow.addView(t4v);

            stk.addView(tbrow);


        }

    }

    @Override
    public void getAbsent(List<Result> absent) {
//                        Toast.makeText(getContext(),response.body().getData().getResult().get(0).getId()+"",Toast.LENGTH_LONG).show();
        p_month.setOnClickListener(v -> compactCalendarView.showPreviousMonth());

        n_month.setOnClickListener(v -> compactCalendarView.showNextMonth());

        month.setText(dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()));

        compactCalendarView.setFirstDayOfWeek(Calendar.MONDAY);

        studentName.setText(absent.get(0).getUser().getName());
        long timeInMilliseconds = 0, timeInMilliNow = 0;
        String currentDateandTime = Tgl.format(new Date());
        String dateStudent = null;
        int color, colorNow = 0;

        for (Result result : absent) {
            try {
                Date mDate = f.parse(result.getTanggal());
                timeInMilliseconds = mDate.getTime();
                Log.d("timeinmiles", timeInMilliseconds + "");
                Now = f.parse(absent.get(0).getTanggal());
                String absen = absent.get(0).getAbsen();
                dateStudent = f.format(Now);

                if (result.getAbsen().equals("hadir") || result.getAbsen().equals("Hadir")) {
                    color = Color.parseColor("#00bbd3");

                } else if (result.getAbsen().equals("sakit") || result.getAbsen().equals("Sakit")) {
                    color = Color.YELLOW;

                } else if (result.getAbsen().equals("izin") || result.getAbsen().equals("Izin")) {
                    color = Color.parseColor("#ababab");

                } else {
                    color = Color.RED;

                }

                Log.d("Tanggal", dateNow + "," + dateStudent);
                if (dateNow.equals(Now)) {
                    if (absen.equals("hadir") || result.getAbsen().equals("Hadir")) {
                        colorNow = Color.parseColor("#00bbd3");
                    } else if (absen.equals("sakit") || result.getAbsen().equals("Sakit")) {
                        colorNow = Color.YELLOW;
                    } else if (absen.equals("izin") || result.getAbsen().equals("Izin")) {
                        colorNow = Color.parseColor("#ababab");
                    } else if (absen.equals("alfa") || result.getAbsen().equals("Alfa")) {
                        colorNow = Color.RED;
                    } else {
                        colorNow = Color.GREEN;
                    }
                } else {
                    colorNow = Color.GREEN;
                }

                compactCalendarView.setCurrentDayBackgroundColor(colorNow);
                compactCalendarView.setCurrentSelectedDayBackgroundColor(colorNow);

                Event ev = new Event(color, timeInMilliseconds, result.getAbsen() + "/" + result.getKet());
                compactCalendarView.addEvent(ev);


                Log.d("samedate", dateStudent + "," + currentDateandTime);


            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void getFinalMark(List<Quize> quize, boolean error) {
        if (error){
            scoreText.setVisibility(View.GONE);
            hrzScroll.setVisibility(View.GONE);
        }

        for (Quize quiz : quize) {
            String tglApi = "yyyy-MM-dd hh:mm:ss";
            String month = "MM";
            SimpleDateFormat inputTgl = new SimpleDateFormat(tglApi);
            SimpleDateFormat outputTgl = new SimpleDateFormat(month);

            String inputPattern = "yyyy-MM-dd hh:mm:ss";
            String outputPattern = "dd MMMM yyyy";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

            Date date = null, dateApi = null;
            String str = null, strTgl = null;

            try {
                dateApi = inputTgl.parse(quiz.getCreated_at());
                strTgl = outputTgl.format(dateApi);
                date = inputFormat.parse(quiz.getCreated_at());
                str = outputFormat.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (strTgl.equals(monthNow)) {
                Log.d("month_now",monthNow+","+strTgl);
                listpelajaran.add(quiz.getTitle());

                nilai.add(quiz.getScore() + "");
            }else{
                stk.setVisibility(View.GONE);
                textExam.setVisibility(View.VISIBLE);
            }

            dateExam.add(str);
        }

        init();
        progress.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        month.setText(dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()));
    }
}
