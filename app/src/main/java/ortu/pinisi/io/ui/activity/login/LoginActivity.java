package ortu.pinisi.io.ui.activity.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.orhanobut.hawk.Hawk;
import com.orhanobut.hawk.NoEncryption;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ortu.pinisi.io.data.apihelper.BaseApiService;
import ortu.pinisi.io.data.apihelper.UtilsApi;
import ortu.pinisi.io.R;
import ortu.pinisi.io.model.login.Login;
import ortu.pinisi.io.model.Constant;
import ortu.pinisi.io.ui.activity.main.MainButtomNav;

public class LoginActivity extends AppCompatActivity implements LoginPresenter.LoginPresenterListener{

    @BindView(R.id.username)
    EditText username;

    @BindView(R.id.pass)
    EditText password;

    @BindView(R.id.login)
    Button submit;

    @BindView(R.id.showpass)
    CheckBox showpassword;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private AdView mAdView;

    ProgressDialog loading;
    Context mContext;
    BaseApiService mApiService;

    String url = null;

    private LoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        mAuth = FirebaseAuth.getInstance();

        mContext = this;

        Hawk.init(LoginActivity.this)
                .setEncryption(new NoEncryption())
                .build();

        Hawk.put(Constant.Key.IS_LOGIN , false);

//        MobileAds.initialize(LoginActivity.this, "ca-app-pub-2941627758407889~4808837650");
//        mAdView = (AdView) findViewById(R.id.ad_view);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();
//        mAdView.loadAd(adRequest);

        firebaseAuth();
    }

    public void firebaseAuth(){
        mAuthListener = firebaseAuth -> {
            FirebaseUser user = firebaseAuth.getCurrentUser();
            if (user != null) {
                // User is signed in
                Log.d("firebase", "onAuthStateChanged:signed_in:" + user.getUid());
            } else {
                // User is signed out
                Log.d("firebase", "onAuthStateChanged:signed_out");
            }
        };
    }

    @OnClick(R.id.showpass)
    public void onShowPassword(View view) {
        if (showpassword.isChecked()) {
            password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {

            password.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
    }

    @OnClick(R.id.login)
    public void onLoginClicked(View view) {
        if ( username.getText().toString().equals("") && password.getText().toString().equals("") ){
            username.setError("Username Belum Di isi");
            password.setError("Password belum di isi");
        }
        else if ( username.getText().toString().equals("") ) username.setError("Username Belum Di isi");
        else if( password.getText().toString().equals("") ) password.setError("Password belum di isi");
        else {
            if (url == null){
                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(LoginActivity.this);
                final View mView = layoutInflaterAndroid.inflate(R.layout.url_layout, null);
                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(LoginActivity.this);
                alertDialogBuilderUserInput.setView(mView);

                final TextView textUrl = (TextView) mView.findViewById(R.id.textUrl);
                Button btnSubmit = (Button) mView.findViewById(R.id.submit);

                btnSubmit.setOnClickListener(v -> {
                    url = textUrl.getText().toString();
                    mApiService = UtilsApi.getAPIService(LoginActivity.this,url); // meng-init yang ada di package apihelper

                    Hawk.put(Constant.Key.KEY_URL,url);

                    loading = ProgressDialog.show(mContext, null, "Harap Tunggu...", true, false);
                    requestLogin();
                });

                AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                alertDialogAndroid.show();
            }else{
                loading = ProgressDialog.show(mContext, null, "Harap Tunggu...", true, false);
                requestLogin();
            }
        }
    }

    private void requestLogin() {
        final String email = username.getText().toString();
        String pass = password.getText().toString();
        loginPresenter = new LoginPresenter(LoginActivity.this, this,mApiService,url, email, pass);
        loginPresenter.getUser();
    }

    @Override
    public void loginRespone(Login login) {
        if (login.getError().equals("FALSE")){

            getUserDatas(login);

            Toast.makeText(mContext, "Login Berhasil ", Toast.LENGTH_SHORT).show();

            startActivity(new Intent(LoginActivity.this, MainButtomNav.class));
            finish();
            loading.dismiss();
        }else{
            loading.dismiss();
            String error_message = login.getError_msg();
            Toast.makeText(mContext, error_message, Toast.LENGTH_SHORT).show();
        }
    }

    public void getUserDatas(Login login){
        Hawk.put(Constant.Key.IS_LOGIN, true);
        Hawk.put(Constant.Key.KEY_ROLE , login.getRole());
        Hawk.put(Constant.Key.KEY_USER_ID , login.getUid());
        Hawk.put(Constant.Key.KEY_NAME , login.getUser().getName());
        Hawk.put(Constant.Key.KEY_EMAIL , login.getUser().getEmail());
        Hawk.put(Constant.Key.KEY_TOKEN , login.getToken());
        Hawk.put(Constant.Key.KEY_CHILD_ID , login.getUser().getChildId());
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    public void onBackPressed() {
        url = null;
        Hawk.deleteAll();
        finish();
    }

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    /**
     * Called when returning to the activity
     */
    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    /**
     * Called before the activity is destroyed
     */
    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }
}