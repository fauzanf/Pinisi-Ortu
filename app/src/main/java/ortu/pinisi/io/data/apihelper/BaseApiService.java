package ortu.pinisi.io.data.apihelper;

import io.reactivex.Flowable;
import io.reactivex.Single;
import okhttp3.ResponseBody;
import ortu.pinisi.io.model.absent.Absent;
import ortu.pinisi.io.model.login.Login;
import ortu.pinisi.io.model.studentabsen.Student;
import ortu.pinisi.io.model.studentannouncements.StudentAnnouncement;
import ortu.pinisi.io.model.studentfinalmark.StudentQuizResult;
import ortu.pinisi.io.model.studentoption.StudentOption;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by mfahm on 11/24/2017.
 */

public interface BaseApiService {

    @FormUrlEncoded
    @POST("api/post/?model=login&type=android")
    Flowable<Login> loginRequest(@Field("username") String username,
                                 @Field("password") String password);

    @GET("api/getdailyabsent")
    Call<Student> getData(@Query("tgl") String tgl,
                          @Query("user_id") String child_id,
                          @Query("start") String start,
                          @Query("rows") String rows,
                          @Query("token") String token);

    @GET("api/getstudentquizes")
    Flowable<StudentQuizResult> getFinalMark(@Query("user_id") String user_id,
                                             @Query("year") String year,
                                             @Query("semester") String semester,
                                             @Query("token") String token);

    @GET("api/getstudentquizes")
    Flowable<StudentQuizResult> getStudentQuize(@Query("user_id") String user_id,
                                                @Query("year") String year,
                                                @Query("semester") String semester,
                                                @Query("token") String token,
                                                @Query("created_at") String created_at);

    @GET("api/list/lesson")
    Flowable<StudentOption> getOption(@Query("token") String token,
                                      @Query("model") String model);

    @GET("api/getannouncements")
    Flowable<StudentAnnouncement> getAnnouncement(@Query("token") String token);

    @GET("api/getstudentfinalmark")
    Flowable<ResponseBody> getStudentFinalMark(@Query("user_id") String user_id,
                                           @Query("year") String year,
                                           @Query("semester") String semester,
                                           @Query("token") String token);

    @GET("api/list?model=absensi_siswa")
    Flowable<Absent> getAbsent(@Query("token") String token,
                               @Query("semester") String semester,
                               @Query("tahun_ajaran") String tahun,
                               @Query("user_id") String userId,
                               @Query("created_at") String created_at);
}
