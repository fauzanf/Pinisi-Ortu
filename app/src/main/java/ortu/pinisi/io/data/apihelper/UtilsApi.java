package ortu.pinisi.io.data.apihelper;

import android.content.Context;

/**
 * Created by mfahm on 11/24/2017.
 */

public class UtilsApi {
    public static final String BASE_URL_API = "http://";

    // Mendeklarasikan Interface BaseApiService
    public static BaseApiService getAPIService(Context context, String url){
        return RetrofitClient.getClient(BASE_URL_API+url+"/",context).create(BaseApiService.class);
    }
}
