package ortu.pinisi.io.utils;

import android.content.Context;
import android.util.Log;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.orhanobut.hawk.Hawk;

import org.json.JSONException;
import org.json.JSONObject;

import ortu.pinisi.io.model.Constant;

public class util {
    public static void setMixPanel(Context context, String name) {
        String userId = Hawk.get(Constant.Key.KEY_USER_ID);
        String school = Hawk.get(Constant.Key.KEY_SCHOOL);
        String distinctId = userId + "_" + school;
        String projectToken = "0f845036711aa41c9e66dcb6f7e02641"; // e.g.: "1ef7e30d2a58d27f4b90c42e31d6d7ad"

        distinctId = distinctId.replace(" ", "_");

        MixpanelAPI mixpanel = MixpanelAPI.getInstance(context, projectToken);
        mixpanel.identify(distinctId);
        mixpanel.getPeople().identify(distinctId);

        try {
            JSONObject props = new JSONObject();
            props.put("Nama", Hawk.get(Constant.Key.KEY_NAME));
            props.put("Email", Hawk.get(Constant.Key.KEY_EMAIL));
            props.put("Sekolah", school);

            Log.i("MixpanelActivity", "" + name + "," + distinctId);
            mixpanel.track("" + name, props);
        } catch (JSONException e) {
            Log.e("MYAPP", "Unable to add properties to JSONObject", e);
        }
    }
}
