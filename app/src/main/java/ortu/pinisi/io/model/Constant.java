package ortu.pinisi.io.model;

/**
 * Created by mfahm on 12/21/2017.
 */

public interface Constant {

    class Key{
        public static String IS_LOGIN = "login";
        public static String KEY_CONTENT = "content";
        public static String KEY_URL = "url";

        public static String KEY_TOKEN = "token";
        public static String KEY_CHILD_ID = "child_id";
        public static String KEY_USER_ID = "user_id";
        public static String KEY_NAME = "name";
        public static String KEY_EMAIL = "email";
        public static String KEY_ROLE = "role";

        public static String KEY_SCHOOL = "school";
        public static String KEY_SEMESTER = "semester";
        public static String KEY_TAHUN_AJARAN = "tahunajaran";
    }

}
