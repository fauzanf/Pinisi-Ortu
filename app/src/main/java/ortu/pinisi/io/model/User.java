package ortu.pinisi.io.model;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by Ojan on 7/7/2017.
 */

@IgnoreExtraProperties
public class User {

    public String email;
    public String noktp;
    public String nama;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNoktp() {
        return noktp;
    }

    public void setNoktp(String noktp) {
        this.noktp = noktp;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String alamat;

    public User() {
    // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

//    public User(String noktp,String nama, String alamat, String email) {
//        this.noktp = noktp;
//        this.nama = nama;
//        this.email = email;
//        this.alamat = alamat;
//    }

}