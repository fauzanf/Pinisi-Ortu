
package ortu.pinisi.io.model.studentfinalmark;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Quize {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("created_at")
    @Expose
    private String created_at;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("right_answer")
    @Expose
    private String rightAnswer;
    @SerializedName("wrong_answer")
    @Expose
    private String wrongAnswer;
    @SerializedName("unanswered")
    @Expose
    private String unanswered;
    @SerializedName("essay_score")
    @Expose
    private Object essayScore;
    @SerializedName("pg_score")
    @Expose
    private Object pgScore;
    @SerializedName("score")
    @Expose
    private String score;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRightAnswer() {
        return rightAnswer;
    }

    public void setRightAnswer(String rightAnswer) {
        this.rightAnswer = rightAnswer;
    }

    public String getWrongAnswer() {
        return wrongAnswer;
    }

    public void setWrongAnswer(String wrongAnswer) {
        this.wrongAnswer = wrongAnswer;
    }

    public String getUnanswered() {
        return unanswered;
    }

    public void setUnanswered(String unanswered) {
        this.unanswered = unanswered;
    }

    public Object getEssayScore() {
        return essayScore;
    }

    public void setEssayScore(Object essayScore) {
        this.essayScore = essayScore;
    }

    public Object getPgScore() {
        return pgScore;
    }

    public void setPgScore(Object pgScore) {
        this.pgScore = pgScore;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

}
