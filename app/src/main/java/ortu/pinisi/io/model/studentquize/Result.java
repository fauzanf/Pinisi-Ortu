
package ortu.pinisi.io.model.studentquize;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("Tidak Mencontek saat ujian")
    @Expose
    private String tidakMencontekSaatUjian;
    @SerializedName("Sikap Spiritual - Predikat")
    @Expose
    private String sikapSpiritualPredikat;
    @SerializedName("Sikap Spiritual - Deskripsi")
    @Expose
    private String sikapSpiritualDeskripsi;
    @SerializedName("Sikap Sosial - Predikat")
    @Expose
    private String sikapSosialPredikat;
    @SerializedName("Sikap Sosial - Deskripsi")
    @Expose
    private String sikapSosialDeskripsi;
    @SerializedName("Absensi - Sakit")
    @Expose
    private String absensiSakit;
    @SerializedName("Absensi - Izin")
    @Expose
    private String absensiIzin;
    @SerializedName("Absensi - Alfa")
    @Expose
    private String absensiAlfa;
    @SerializedName("Prestasi 1 - Kegiatan")
    @Expose
    private String prestasi1Kegiatan;
    @SerializedName("Prestasi 1 - Keterangan")
    @Expose
    private String prestasi1Keterangan;
    @SerializedName("Catatan WaliKelas")
    @Expose
    private String catatanWaliKelas;
    @SerializedName("Prestasi 2 - Kegiatan")
    @Expose
    private String prestasi2Kegiatan;
    @SerializedName("Prestasi 2 - Keterangan")
    @Expose
    private String prestasi2Keterangan;
    @SerializedName("Prestasi 3 - Kegiatan")
    @Expose
    private String prestasi3Kegiatan;
    @SerializedName("Prestasi 3 - Keterangan")
    @Expose
    private String prestasi3Keterangan;
    @SerializedName("Prestasi 4 - Kegiatan")
    @Expose
    private String prestasi4Kegiatan;
    @SerializedName("Prestasi 4 - Keterangan")
    @Expose
    private String prestasi4Keterangan;
    @SerializedName("Ekstrakurikuler 1 - Nama")
    @Expose
    private String ekstrakurikuler1Nama;
    @SerializedName("Ekstrakurikuler 1 - Nilai")
    @Expose
    private String ekstrakurikuler1Nilai;

    public String getTidakMencontekSaatUjian() {
        return tidakMencontekSaatUjian;
    }

    public void setTidakMencontekSaatUjian(String tidakMencontekSaatUjian) {
        this.tidakMencontekSaatUjian = tidakMencontekSaatUjian;
    }

    public String getSikapSpiritualPredikat() {
        return sikapSpiritualPredikat;
    }

    public void setSikapSpiritualPredikat(String sikapSpiritualPredikat) {
        this.sikapSpiritualPredikat = sikapSpiritualPredikat;
    }

    public String getSikapSpiritualDeskripsi() {
        return sikapSpiritualDeskripsi;
    }

    public void setSikapSpiritualDeskripsi(String sikapSpiritualDeskripsi) {
        this.sikapSpiritualDeskripsi = sikapSpiritualDeskripsi;
    }

    public String getSikapSosialPredikat() {
        return sikapSosialPredikat;
    }

    public void setSikapSosialPredikat(String sikapSosialPredikat) {
        this.sikapSosialPredikat = sikapSosialPredikat;
    }

    public String getSikapSosialDeskripsi() {
        return sikapSosialDeskripsi;
    }

    public void setSikapSosialDeskripsi(String sikapSosialDeskripsi) {
        this.sikapSosialDeskripsi = sikapSosialDeskripsi;
    }

    public String getAbsensiSakit() {
        return absensiSakit;
    }

    public void setAbsensiSakit(String absensiSakit) {
        this.absensiSakit = absensiSakit;
    }

    public String getAbsensiIzin() {
        return absensiIzin;
    }

    public void setAbsensiIzin(String absensiIzin) {
        this.absensiIzin = absensiIzin;
    }

    public String getAbsensiAlfa() {
        return absensiAlfa;
    }

    public void setAbsensiAlfa(String absensiAlfa) {
        this.absensiAlfa = absensiAlfa;
    }

    public String getPrestasi1Kegiatan() {
        return prestasi1Kegiatan;
    }

    public void setPrestasi1Kegiatan(String prestasi1Kegiatan) {
        this.prestasi1Kegiatan = prestasi1Kegiatan;
    }

    public String getPrestasi1Keterangan() {
        return prestasi1Keterangan;
    }

    public void setPrestasi1Keterangan(String prestasi1Keterangan) {
        this.prestasi1Keterangan = prestasi1Keterangan;
    }

    public String getCatatanWaliKelas() {
        return catatanWaliKelas;
    }

    public void setCatatanWaliKelas(String catatanWaliKelas) {
        this.catatanWaliKelas = catatanWaliKelas;
    }

    public String getPrestasi2Kegiatan() {
        return prestasi2Kegiatan;
    }

    public void setPrestasi2Kegiatan(String prestasi2Kegiatan) {
        this.prestasi2Kegiatan = prestasi2Kegiatan;
    }

    public String getPrestasi2Keterangan() {
        return prestasi2Keterangan;
    }

    public void setPrestasi2Keterangan(String prestasi2Keterangan) {
        this.prestasi2Keterangan = prestasi2Keterangan;
    }

    public String getPrestasi3Kegiatan() {
        return prestasi3Kegiatan;
    }

    public void setPrestasi3Kegiatan(String prestasi3Kegiatan) {
        this.prestasi3Kegiatan = prestasi3Kegiatan;
    }

    public String getPrestasi3Keterangan() {
        return prestasi3Keterangan;
    }

    public void setPrestasi3Keterangan(String prestasi3Keterangan) {
        this.prestasi3Keterangan = prestasi3Keterangan;
    }

    public String getPrestasi4Kegiatan() {
        return prestasi4Kegiatan;
    }

    public void setPrestasi4Kegiatan(String prestasi4Kegiatan) {
        this.prestasi4Kegiatan = prestasi4Kegiatan;
    }

    public String getPrestasi4Keterangan() {
        return prestasi4Keterangan;
    }

    public void setPrestasi4Keterangan(String prestasi4Keterangan) {
        this.prestasi4Keterangan = prestasi4Keterangan;
    }

    public String getEkstrakurikuler1Nama() {
        return ekstrakurikuler1Nama;
    }

    public void setEkstrakurikuler1Nama(String ekstrakurikuler1Nama) {
        this.ekstrakurikuler1Nama = ekstrakurikuler1Nama;
    }

    public String getEkstrakurikuler1Nilai() {
        return ekstrakurikuler1Nilai;
    }

    public void setEkstrakurikuler1Nilai(String ekstrakurikuler1Nilai) {
        this.ekstrakurikuler1Nilai = ekstrakurikuler1Nilai;
    }

}
