
package ortu.pinisi.io.model.studentoption;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("totalPage")
    @Expose
    private Integer totalPage;
    @SerializedName("perPage")
    @Expose
    private Integer perPage;
    @SerializedName("result")
    @Expose
    private List<Result> result = null;

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public Integer getPerPage() {
        return perPage;
    }

    public void setPerPage(Integer perPage) {
        this.perPage = perPage;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

}
