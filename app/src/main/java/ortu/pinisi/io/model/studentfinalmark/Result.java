
package ortu.pinisi.io.model.studentfinalmark;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ortu.pinisi.io.model.*;

public class Result {

    @SerializedName("nisn")
    @Expose
    private String nisn;
    @SerializedName("display_name")
    @Expose
    private String displayName;
    @SerializedName("class")
    @Expose
    private String _class;
    @SerializedName("school_name")
    @Expose
    private String schoolName;
    @SerializedName("quizes")
    @Expose
    private List<Quize> quizes = null;

    public String getNisn() {
        return nisn;
    }

    public void setNisn(String nisn) {
        this.nisn = nisn;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getClass_() {
        return _class;
    }

    public void setClass_(String _class) {
        this._class = _class;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public List<Quize> getQuizes() {
        return quizes;
    }

    public void setQuizes(List<Quize> quizes) {
        this.quizes = quizes;
    }

}
